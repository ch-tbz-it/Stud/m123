# Befehlsreferenz

Sammlung der wichtigsten MikroTik und Linux Befehle

# (Debian flavored) Linux commands
| Command | Description | 
| ------- | ----------- | 
| nano | A command-line text editor | 
| apt install | Installs a package using the APT package manager | 
| ping | Sends ICMP echo requests to test network connectivity | 
| cat | Displays the contents of a file or concatenates multiple files | 
| touch | Creates a new, empty file or updates the timestamp of an existing file | 
| ls | Lists the files and directories in the current directory | 
| pwd | Shows the current working directory | 
| cd | Changes the current working directory | 
| mkdir | Creates a new directory | 
| mv | Moves or renames files | 
| cp | Copies files  

Mehr Befehle auf den folgenden *Cheat Sheet*:
 - https://www.loggly.com/wp-content/uploads/2015/05/Linux-Cheat-Sheet-Sponsored-By-Loggly.pdf
 - https://www.debian.org/doc/manuals/refcard/refcard

## MikroTik

### Aufbau der CLI
Siehe:
 - https://help.mikrotik.com/docs/display/ROS/Console

### IPv4 Befehle
#### Links:
 - https://help.mikrotik.com/docs/display/ROS/IPv4+and+IPv6+Fundamentals
 - https://help.mikrotik.com/docs/display/ROS/IP+Routing

#### Wichtigste Befehle: 
 - Auf Interfaces konfigurierte IPv4-Adressen anzeigen: `/ip address/print`
 - *Beispiel:* Auf einem bestimmtem Interface eine IPv4-Adresse konfigurieren: `/ip address/add interface=ether2 address=192.168.5.1/24`
 - *Beispiel:* Eine Konfigurierte IP-Adresse löschen
   - 1. Nummer des zu löschenden Eintrages mithilfe von `/ip address/print` herausfinden (Steht in der Spalte `#`). 
   - 2. Nachfolgenden Befehle eingeben. `X` durch entsprechende Nummer ersetzen: `/ip address/remove numbers=X` 
 - Alle aktiven Routen anzeigen: `/ip route/print` 
 - *Beispiel:* Statische Route hinzufügen: `/ip route/add dst-address=192.168.90.0/24 gateway=192.168.5.2`

### DHCP-Server
#### Links:


#### Wichtigste Befehle: 
 - Eine neuen DHCP Server konfigurieren: `/ip dhcp-server/setup`


### DNS-Server
#### Links:
- [DNS - MikroTik Documentation](https://help.mikrotik.com/docs/display/ROS/DNS)

#### Wichtigste Befehle:
- DNS-Server konfigurieren: `/ip dns set servers=[DNS Server IP] allow-remote-requests=yes`
- DNS-Static-Eintrag hinzufügen: `/ip dns static add name=example.com address=192.168.5.5`
- DNS-Cache anzeigen: `/ip dns cache print`
- DNS-Cache leeren: `/ip dns cache flush`

### DHCP-Client
#### Links:
- [DHCP Client - MikroTik Documentation](https://help.mikrotik.com/docs/display/ROS/DHCP+Client)

#### Wichtigste Befehle:
- DHCP-Client auf einem Interface aktivieren: `/ip dhcp-client add interface=[Interface]`
- DHCP-Client Informationen anzeigen: `/ip dhcp-client print detail`
