# Netzwerkprotokolle zur Dateiübertragung

Während auf HTTP basierte Dateiübertragung wie Nextcloud, Goole Drive, OneDrive, iCloud im privaten und zunehmend auch im Geschäftsumfeld eine grössere Rolle einnehmen, spielen die Netzwerkübertragungsprokolle SMB (CIFS), NFS, FTP, iSCSI weiterhin eine wichtige Rolle. Für IT-Fachpersonen spielt zudem das TFTP Protokoll aufgrund dessen Einfachheit eine wichtige Rolle. Nicht vergessen werden darf SSH, dass nebst dem Remote Terminal auch eine verschlüsselte Dateiübertragung (SFTP) bietet. 

Im nachfolgenden Laborübungen probieren sie einen Teil dieser Protokolle aus. Die verwendeten Softwareprodukte implementieren weit mehr Protokolle, als in der Übung abgedeckt werden. Scheuen Sie sich nicht, lassen Sie Ihrer Neugier freien lauf und probieren Sie aus!

# Theorie

## TFTP
Das Trivial File Transfer Protocol (TFTP) ist ein - wie es der Name bereits andeutet - ein simpels auf UDP basierendes Dateiübertragungsprotokoll. 
https://en.wikipedia.org/wiki/Trivial_File_Transfer_Protocol


https://en.wikipedia.org/wiki/Trivial_File_Transfer_Protocol

## Lernziele
 - Primärer Einsatzzweck der Dateiübertragungsprotkolle
 - Sicherheit der Dateiübertragungsprotkolle: 
   - Welche Protokolle sind standardmässig verschlüsselt? 
   - Welche haben eine Verschlüsselungsoption? 
   - Welche sind immer unverschlüsselt?
 - Welche Protokolle verwenden welche Standardports?
 - Geschichtlicher Hintergrund: Für welchen Zweck wurden die Protokolle ursprünglich entwickelt?
 - Wie unterscheidet sich 
 - Zu kennende Protokolle: TFTP, FTP, SFTP, SMB (CIFS)

# Laborübungen
Die nachfolgenden Laborübungen bauen aufeinander auf. Denken Sie daran die Laborübungen gemäss Anweisungen in [02_Lernjournal](../02_Lernjournal) zu dokumentieren.

## Laborübung 1: TFTP - Cisco Router Backup
[Auf zum Labor 1](01_TFTP.md)

## Laborübung 2: TrueNAS - SMB / FTP mit Benutzerverwaltung
[Auf zum Labor 2](02_NAS.md)