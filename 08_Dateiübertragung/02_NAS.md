# Laborübung 2: SMB - Lokale Dateiablage auf einem NAS

In dieser Laborübung wird ein kleines Netzwerk mit einem [NAS](https://en.wikipedia.org/wiki/Network-attached_storage) (Network-attached storage), einem Windows 10 Client und einem Ubuntu Client aufgebaut. Der Zugriff auf die Dateiablage erfolgt mithilfe von [SMB](https://en.wikipedia.org/wiki/Server_Message_Block). 

## Anwendungsfall
![Netzwerktopologie der Firma](media/Firmennetzwerk.PNG)

Die kleine Firma `Klein aber Oho AG` entwickelt Windows-Applikationen. Das Unternehmen besteht aus drei Mitarbeiter: Eine Chefin und zugleich Entwicklerin, eine weitere Entwicklerin und die Sekretärin, die zugleich Buchhalterin ist. Die Firma hat von Microsoft *Microsoft 365 Business Standard* im Abo für jeden Benutzer. Von den vielzahl angebotenen Funktionen verwenden sie es vor allem für E-Mail, Kollaborative Tools und als Dateiablage. Am Firmenstandort in Zürich hat die Firma ein kleines Büro mit ein paar Arbeitsplätzen. In der Netzwerktopologie ist zu erkennen, dass die Firma folgende Geräte hat:
 - **Router Firewall**, die nebst dem Routing-, Gateway- und Firewall-Funktionen, auch den DHCP, DNS und VPN-Server bereitstellt. 
 - **Managed PoE Switch**, der nebst der grundlegenden *Switching*-Funktion, auch über PoE dem Access Point und dem IP-Telefon die Versorgungsspannung liefert. 
 - Pro Mitarbeiter ein **leistungsstarker PC** auf den die Mitarbeiter vor Ort oder via VPN zugreifen können. 
 - Pro Mitarbeiter ein **Notebook**, den Sie vor Ort, unterwegs oder im Home-Office verwenden. 
 - Für die Chefin ein physisches **IP-Telefon**. Die anderen Mitarbeiter telefonieren nur über den Softclient (z.B. Teams)
 - Ein **Drucker** für alle Mitarbeiter
 - Ein **Access Point** der für das ganze Büro ausreicht.

Ein Teil ihrer Dateien möchten die Firma aus Performance und Datenschutzgründen auf einem lokalen NAS abspeichern. Zusätzlich möchte Sie aus dem NAS ein Backup aller Daten machen, die in der Cloud gespeichert sind. Auf dem *Fileshare*/Dateiablage möchte möchte die Firma drei Fileshares machen mit unterschiedlichen Berechtigungen. Dazu kommt auch noch ein Fileshare für das Backup, auf welches nur mit dem Administartor zugegriffen werden kann. 

**Weitere Anforderungen:**
 - HDD Ausfallsicherung (Gespiegelt)
 - 4 TB nutzbarer Speicher
 - Mindestens 100 MB/s Datendurchsatz pro Benutzer bei gleichzeitiger Verwendung
 - Zugriff via SMB

Die Chefin stellt die nachfolgende Berechtigungsmatrix bereit: 

![Berechtigungsmatrix](media/Berechtigungsmatrix.PNG)

## Auftrag
 - Evaluieren Sie ein entsprechendes Gerät und die dazu passende Software, welches die genannten Anforderungen erfüllt.
 - Erstellen Sie ein Angebot. Beschränken Sie sich dabei auf
   - die Anpassung des Netzwerktopologie Planes
   - Die Hardware mit den für den Kunden wichtigsten Merkmalen, die seine Anforderungen erfüllen
   - Arbeitsaafwandsabschätzung und die wichtigsten Milestones
   - Bestandteile, wie Anschrift, Gestaltung der Offerte, Erstellen eines ausdruckbaren Dokumentes weglassen
 - Setzten sie das Netzwerk in vereinfachter Form in GNS3 um. 

## Lernziele
 - Kennt die Einstellungen zur Konfiguration eines File Servers und kann aufzeigen, wie die Einstellgrössen die physische Datenablage (Datenträger, Verzeichnisse), die Nutzung der Datenablage (z.B. Volumen) oder den Datenzugriff (z.B. Rechte) im Netzwerk beeinflussen.
 - Kennt Kriterien (gemeinsame Nutzung, Datensicherheit/-Informationssicherheit Sensitivität etc.), die bei der Konzeption der Datenablage in einem Netzwerk zu beachten sind und kann erläutern, wie diese Kriterien den Entscheid, welche Daten zentral resp. dezentral abgelegt werden, beeinflussen.
 - Kennt die Möglichkeiten eines Server-Betriebssystems zur Gewährleistung und Absicherung des Zugriffs auf Netzwerk-Ressourcen (Benutzer-Authentifizierung, Benutzer-Autorisierung, Ressourcenanbindung) und kann an Beispielen erläutern, wie damit die Dienste, Konfigurationsdateien sowie weitere Daten vor unerlaubtem Zugriff geschützt werden können.

## Umsetzung in GNS3

![GNS3 NAS](media/GNS3_NAS.PNG)

### Anforderungen NAS
 - *System image*: FreeNAS, TrueNAS oder manuell Konfiguriert auf einem Debian ähnlichen Betriebssystem
 - Eine fixe IP Adresse ausserhalb des DHCP Bereichs
 - 3 SMB-Shares (Administration, Buchhaltung, Entwicklung)
 - Pro SMB Share eine Berechtigungsgruppe (ACL Administration, ACL Buchhaltung, usw. )
 - Für jeden Mitarbeiter einen Benutzernamen mit Zugang zu den entsprechenden Shares gemäss Berechtigungsmatrix

### Anforderungen Test-Clients
 - Ein *Ubuntu Desktop Client* mit Zugang zum NAS
 - Der *Ubuntu Desktop Client* ist als Entwicklerin im NAS eingeloggt
 - Ein *Windows Client* mit Zugang zum NAS
 - Der *Windows Client* ist als Buchhalterin im NAS eingeloggt
 - Entsprechende Verknüpfungen zum NAS sind an nützlichen Stellen in der Benutzeroberfläche abgelegt
 - Die Zugangsdaten zum NAS sind abgespeichert und müssen nach einem Neustart nicht erneut eingegeben haben
 
### Dokumentation
 - Liste mit Initialpasswörter für die Benutzer
 - Angepasste Netzwerktopologie
 - Getätigte Konfiguration mit den relevanten Parameter
 - Links zu den verwendeten Dokumentationen und Anleitungen

Wenn ein anderer Lernender mithilfe Ihrer Dokumentation ohne zusätzliche Hinweise das Labor 1:1 nachbauen kann, dann ist das ein mögliches Indiz für eine gelungene Dokumentation. 

### Hinweis
 - Wenn das GNS3 Labor mit anderen Lernenden geteilt wird, muss die IP-Adresse des Management-Interfaces auf dem Router abgesprochen werden. Ein Lernender verwendet z.B. die IP-Adresse `192.168.23.15/24` und der andere `192.168.23.16/24`.

### Mögliche Erweiterungen (Zusatzaufgaben)
 - Auf Router Firewall einen VPN Server einrichte mit WireGuard oder OpenVPN mit einem entsprechenden Test-Client
