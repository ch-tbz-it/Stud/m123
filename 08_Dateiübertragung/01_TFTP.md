# Laborübung 1: TFTP

In dieser Laborübung werden wir die Konfiguration eines Cisco Routers über TFTP sichern, auf dem eigenen PC bearbeiten und anschliessend wieder auf den Router hochladen. 

# Einleitung
Das Trivial File Transfer Protocol (TFTP) ist ein - wie es der Name bereits andeutet - ein simpels auf UDP basierendes Dateiübertragungsprotokoll. Aufgrund des einfachen Aufbaus des Protokolls und des minimalen Funktionsumfangs (Datei senden, Datei empfangen) wird es sehr gerne verwendet, um im «Boot-Modus» (Wie der Modus bezeichnet wird, ist abhängig vom jeweiligen Gerät) von *Embedded Devices* Firmware und/oder Konfigurationsdateien in das Gerät einzuspielen. Dieser kurze Text versucht zu erklären, weshalb ein so simples - und für den Laien nicht ganz einfach zu bedienendes - Protokoll TFTP äusserst nützlich ist und immer noch Verwendung findet. 

In einem modernen Betriebssystem wie Ubuntu Desktop oder Windows stehen uns viele Abstraktionsebenen zur Verfügung, welche die Arbeit der Softwareentwicklung stark vereinfachen. Bei der Entwicklung eines neues TCP basierenden Anwendungsprotokolls in einer Windows WPF Applikation muss sich der Entwickler weder um den Treiber der Netzwerkkarte noch mit dem IP-Routing oder der TCP Verbindungssteuerung beschäftigen. Stattdessen kann der Entwickler auf das Betriebssystem zurückgreifen, dass diese Funktionalität implementiert und der Anwendung eine Schnittstelle zur Verfügung stellt. Wenn ein Computer bootet, beginnt er Instruktionen an einer bestimmten Stelle im Speicher zu lesen. Das ist das erste Programm, dass der Computer lädt. Dieses Programm ist sehr klein und hat einen sehr limitierten Funktionsumfang. Der Funktionsumfang ist abhängig vom Geräte-Typ. Die BIOS- oder UEFI-Konfiguration ist eine bekannte Konfigurationsoberfläche, die es dem Benutzer erlauben z.B. das Start-Laufwerk auszuwählen. Sobald dieses erste Programm den Computer Betriebsbereit gemacht hat, übergibt es die Steuerung dem Betriebssystem. Dieses führt anschliessend den Ladevorgang aus und bereitet alles so vor, damit der Anwender anschliessend seine Applikation ausführen kann. 

Bei einem Router oder managed Switch unterscheidet sich das Prinzip nur wenig. Das Betriebssystem heisst dort meistens Firmware und das BIOS-Konfigurationsmenü ist wesentlich weniger umfangreich. Wie bei einem PC, möchte man jedoch auch bei einem Router die Möglichkeit haben, die Firmware zu ersetzen. Meistens kann das einfach über die UI erfolgen oder die Geräte besitzen sogar eine Auto-Upgrade Funktion. Diese Funktionen sind meist Teil des Betriebssystems des Routers. Doch immer wieder kommt es vor, dass diese Funktion versagt oder ein Downgrade erforderlich ist. Doch wenn das Betriebssystem nicht mehr geladen werden kann, sind wir auf das Programm angewiesen, welches vor dem Betriebssystem bzw. Boot-Vorgang geladen wird. Je grösser der Funktionsumfang dieses Boot-Programmes ist, desto teurer und komplexer ist die benötigte Hardware, die Entwicklung und Wartung. Wenn die großartigen Abstraktionsebenen und Libraries von modernen Betriebssystemen nicht zur Verfügung stehen, kann die Entwicklung und Maintenance einer für uns heutzutage «einfachen» Funktion schnell zu einer äusserst komplexen Angelegenheit werden. Es macht einfach keinen Sinn ein super einfach zu bedienende Benutzeroberfläche zu entwickeln, wenn die Funktion nur selten gebraucht wird und als Zielpublikum Fachspezialisten hat. 

Um die zugrundeliegende Überlegung zu verbildlichen: Angenommen alle Autofahrer führen einen Anhänger mit einer kompletten Autowerkstatt mit Reparaturroboter mit. Es sollte für alle klar sein, dass dies aus Ressourcen und energietechnischer Schicht nicht sinnvoll wäre. Genau so wenig verhältnismässig ist es in einen Router ein «super-fancy» Boot-Programm einzubauen. Für den Use-Case «Firmware im Notfall auf den Router laden» wird deshalb gerne auf das Protokoll TFTP zurückgegriffen, da es simpel zum Implementieren ist und keine grosse Hardwareanforderungen mit sich bringt. 

Da TFTP so einfach aufgebaut ist, kann es auch einfacher nachvollzogen werden und unterstützt Lernende beim Verständnis von Dateiübertragungsprotokollen. 

# Lernziele
Die Lernziele sind im jeweiligen Abschnitt vermerkt. 

# Voraussetzung
 - OpenVPN Verbindung zum GNS3 Labor Server aufgebaut
 - GNS3 mit dem GNS3 Labor Server verbunden
 - GNS3 Projekt importiert
 - In das Thema TFTP eingelesen

# Ausgangslage
![GNS3 TFTP](media/GNS3_TFTP.PNG)

# Aufgaben
Halten Sie alle Antworten und getätigten Konfigurationen in ihrem persönlichen Repository fest. 

# 1. TFTP Server auf lokalem PC starten
## Lernziele
 - Kann auf dem eigenen Gerät ein TFTP Server installieren und starten

## Anweisungen
Damit der Cisco Router seine Konfiguration via TFTP auf einen PC senden kann, benötigen wir eine Applikation, die den *TFTP Service* bzw. *TFTP Server* auf unserem PC bereitstellt. 

Auf https://pjo2.github.io/tftpd64/ kann die Installationsdatei für TFTPD64 heruntergeladen werden. 

Nach der Installation:
 - Auf `C:\TFTP` ein Verzeichnis als zukünftiges *root-*Verzeichnis für den TFTP Server anlegen
 - *TFTPD64* Anwendung starten
 - `Current Directory` auf `C:\TFTP`
 - `Server interfaces` entsprechendes GNS3 interface Auswählen (IP im Subnetz: 192.168.23.0/24)

# 2. Konfiguration via TFTP auf den lokalen PC senden/kopieren
## Lernziele
 - Kann eine Konfigurationsdatei aus einem Router via TFTP auf das eigene Notebook übertragen

## Anweisungen
*In diesem Abschnitt gibt es keine Schritt-für-Schritt Anleitung.*

Ziel: Kopieren der `startup-config` vom Cisco Router auf ihrem PC mithilfe von TFTP. 

**Tipp:** Innerhalb eines Cisco Routers wird die `startup-config` im NVRAM gespeichert. 

**Reminder:** Der verwendete Befehl im persönlichen Lernjournal dokumentieren. 

## Fragen
1. Wann Werden Änderungen an der `startup-config` übernommen?

# 3. Konfiguration bearbeiten
## Lernziele
 - Kann eine Konfigurationsdatei gemäss Anweisungen anpassen und sich das dafür benötigte Wissen selbstständig erarbeiten

## Anweisungen

*In diesem Abschnitt gibt es keine Schritt-für-Schritt Anleitung.*

Aktuell verwendet der Router für das lokale Netzwerk 192.168.4.0/24. 
Neu möchten wir ein anderes Subnetz verwenden. Zur Berechnung des neuen Subnetzes ist folgende Formel zu verwenden:

```
192.168.X.0/24 => X = 24 + [Position in Klassenliste]

Beispiel:
Position in Klassenliste = 5
X = 24 + 5 = 29
==>
192.168.29.0/24

```

Alle Vorkommnisse des Subnetze müssen im File angepasst werden, sodass auch der DHCP Server anschliessend funktioniert. 

**Tipp Code-Editor:** [Visual Studio Code](https://code.visualstudio.com/) oder [Notepad++](https://notepad-plus-plus.org/downloads/) für die Bearbeitung der Konfigurationsdatei verwenden.

# 4. Konfiguration auf den Router uploaden
## Lernziele
 - Kann per TFTP eine Konfiguration auf den Router uploaden

## Anweisungen
*In diesem Abschnitt gibt es keine Schritt-für-Schritt Anleitung.*

Im vorherigen Abschnitt wurde das lokale Subnetz des Routers (LAN) angepasst. Nun soll die angepasst `startup-config` auf den Router *R1* hochgeladen werden. Anschliessend muss der Router neu gestartet werden, damit er die Konfiguration übernimmt. (Befehl: `reload`) 

Mithilfe eines VPCS kann überprüft werden, ob die Anpassung erfolgreich war: Bekommt der VPC die IP-Adresse im richtigen Subnetz?

Weiter kann die erfolgreiche Übernahme der Konfigurationsänderungen im Router mit dem Befehl `show running-config` oder `show ip interface brief` überprüft werden. 

## Fragen
 1. Mit welcher Abfolge von Befehlen kann eindeutig festgestellt werden, dass die Subnetzanpassung erfolgreich war und alle Services (DHCP, DNS, usw.) eindeutig funktionieren?

# 5. TFTP Protokoll beobachten
*In diesem Abschnitt gibt es keine Schritt-für-Schritt Anleitung.*

**Lernziele:**
 - Kann die einzelnen Datenpakete von TFTP in einem *packet capture* erklären
 - Kann Dateien aus einem *packet capture* einer TFTP Dateiübertragung extrahieren


Abgabe: PCAP Datei mit dem Upload der `startup-config`

 - Die PCAP Datei ist im Lernjournal abzulegen. 
 - Die PCAP Datei dark eine weiteren Packete ausser der TFTP Übertragung enthalten.

Versuchen Sie mithilfe von *Wireshark* den Inhalt der `startup-config` zu extrahieren. Entsprechende Anleitungen finden Sie im Internet. Machen Sie vom entsprechenden Fenster einen Screenshot und fügen Sie diesen in Ihrem Lernjournal ein. Im Screenshot muss zur Verifikation ihr konfiguriertes Subnetz sichtbar sein. 