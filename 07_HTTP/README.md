# HTTP - Webserver

## Was ist ein Webserver?

Stellt euch vor, ihr wollt eine Webseite auf eurem Smartphone oder Computer aufrufen, zum Beispiel um die neuesten Nachrichten zu lesen oder in einem Online-Shop einzukaufen. Damit das möglich ist, muss es irgendwo einen Computer bzw. Server geben, der diese Webseite bereitstellt. Genau das ist die Hauptaufgabe eines Webservers: Er stellt Webseiten und Webapplikationen über das HTTP Protokoll bereit (Alternative Protokolle wie [QUIC](https://en.wikipedia.org/wiki/QUIC) haben sich bisher noch nicht durchgesetzt).

Eine mögliche Unterscheidung kann zwischen statischen Webseiten und dynamischen Webseiten gemacht werden:

 - **Statische Webseiten** (Zum Beispiel Webauftritt einer Firma, Informationen über eine Klinik, Tourismus Info Seite, usw.) bestehen aus festen Inhalten, die sich nicht interaktiv ändern: Der Autor der Webseite definiert den Inhalt, welche die Besucher zu sehen bekommen. 

 - **Webapplikationen** (Online-Banking, Online-Shop, usw.) hingegen sind interaktiver und können sich dynamisch aufgrund Benutzerinteraktion verändern. Im Hintergrund sind im Vergleich zu einer statischen Webseite nicht nur Inhalte definiert, sondern auch Abläufe, wie zum Beispiel das Hinzufügen eines Produktes in den Warenkorb, die Bestellung des Warenkorbes oder etwas essentielles, wie das Einloggen des Benutzers. In solchen Fällen wird auf dem Webserver oft zusätzliche Software ausgeführt, die die Seite erst generiert, bevor sie an den Browser geschickt wird.

Die Trennung zwischen Webapplikationen und statischen Webseiten ist vielmehr eine theoretische Trennung. In der Realität beinhalten die meisten als "statisch" eingeteilten Webseiten auch dynamische Inhalte, wie zum Beispiel das Kontaktformular auf einer Firmenwebseite. Auch Webapplikationen enthalten häufig statische Inhalte, die nur durch den Autor der Webseite oder teilweise sogar nur durch die Entwickler der Webapplikation angepasst werden. 

## Wie kommunizieren Webbrowser und Webserver?

Wenn ihr eine Webseite in eurem Webbrowser aufruft, sendet der Browser eine Anfrage an den Webserver. Diese Anfrage und die Antwort des Servers erfolgen in der Regel über das Protokoll HTTP(s). 

### HTTP (HyperText Transfer Protocol)

HTTP ist ein Protokoll, das die Kommunikation zwischen Webbrowsern und Webservern regelt. Es ist dafür zuständig, dass der Browser dem Server mitteilt, welche Informationen er anfordert, und dass der Server diese Informationen dann an den Browser sendet. HTTP nutzt standardmäßig den Port 80.

### HTTPS (HyperText Transfer Protocol Secure)

HTTPS ist im Grunde dasselbe wie HTTP, allerdings mit einer zusätzlichen Sicherheitsschicht. Es verschlüsselt die Daten, die zwischen dem Webbrowser und dem Webserver hin- und hergeschickt werden. Das ist besonders wichtig, wenn sensible Daten, wie Passwörter oder Kreditkarteninformationen, übertragen werden. HTTPS nutzt standardmäßig den Port 443. Inzwischen hat sich HTTPS durchgesetzt. Die unverschlüsselte Übertragung von Webseiten gilt heute nicht mehr als zeitgemäss. Dank Organisationen wie [Let's Encrypt](https://letsencrypt.org/) ist es für jeden möglich seine Webseite über HTTPS abzusichern. 

### Welche Protokolle liegen darunter?
Die Kommunikation zwischen Webbrowsern und Webservern findet über ein Netzwerk, normalerweise das Internet, statt. Um diese Kommunikation zu ermöglichen, werden verschiedene Protokolle auf unterschiedlichen Schichten genutzt:

 - IP (Internet Protocol): IP (IPv4 oder IPv6) ist das Vermittlungsprotokoll (OSI-Modell Layer 3) im Internet und gehört zum *Network Layer*. 
 - TCP (Transmission Control Protocol): TCP gehört zur Transportschicht (OSI-Modell Layer 4) und sorgt dafür, dass eine zuverlässige Verbindung zwischen Webbrowser und Webserver besteht. Es teilt die Daten in kleinere Pakete auf, die dann über das Internet versendet werden und stellt sicher, dass alle Pakete korrekt und in der richtigen Reihenfolge ankommen.
 - Auf Layer 2 kommen insbesondere bei der Übertragung über das Internet verschiedene Protokolle zum Einsatz. In einem einfachen KMU- oder Heimnetzwerk ist häufig *Ethernet II* zu finden. 

### HTTP - Ein Dateiübertragungsprotokoll
Das HTTP-Protokoll wird in erster Linie mit Webseiten bzw. dem Webbrowser und Webservern in Verbindung gebracht. Doch ganz grundsätzlich ist HTTP ein Dateiübertragungsprotokoll bzw. ein universelles Datenübertragungsprotokoll. HTTP ist universell und wird für alle möglichen Anwendungszwecke eingesetzt: *Apache2* mit statischen HTML-,JPEG-,CSS-,JS-Dateien, *NGINX* verknüpft über WSGI mit einer Python-Applikation, *Spring Boot*-Applikation in einem Docker-Container, der nach außen über einen Load Balancer und SSL-Termination mit *HAProxy* verbunden ist, *Azure API* zum automatischen Deployment von *Azure Functions*. HTTP ist eines der wichtigsten Protokolle, und es ist deshalb für Informatiker von zentraler Bedeutung, ein grundlegendes Verständnis dafür zu haben.

# Theoretische Grundlagen
Als Vorbereitung für die Laborübung werden folgende Ressourcen empfohlen: 
 - https://de.wikipedia.org/wiki/Hypertext_Transfer_Protocol
 - https://www.youtube.com/watch?v=wW2A5SZ3GkI
 - https://www.youtube.com/watch?v=iYM2zFP3Zn0 (nur die ersten 15 Minuten)
 - https://www.freecodecamp.org/news/what-is-http/
 - https://www.cloudflare.com/learning/ddos/glossary/hypertext-transfer-protocol-http/
 - https://developer.mozilla.org/en-US/docs/Web/HTTP/Overview

Weitere PDFs mit Lernressourcen stehen in der Dokumentenablage zum Modul (Meistens in der MS Teams Gruppe unter Allgemein -> Dateien/Dokumente -> Kursmaterialien) zur Verfügung.

# Laborübungen

# Laborübung: Der eigene Webserver (Grundlagen)
[Auf zum Labor (HTTP) ](HTTP_Webserver/README.md)

# Laborübungen mit GNS3 (Fortgeschritten)
Die nachfolgenden Laborübungen bauen aufeinander auf. Denken Sie daran die Laborübungen gemäss Anweisungen in [02_Lernjournal](../02_Lernjournal) zu dokumentieren, sofern vom Kursleiter / Lehrperson gefordert.

## Laborübung 1: 
[Auf zum Labor (GNS3) ](HTTP_GNS3/01_README.md)