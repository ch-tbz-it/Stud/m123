# Zusatzaufgaben

Die Zusatzaufgaben decken weiterführende Handlungskompetenzen ab und dienen als Ergänzung zu den den Übungen. 

Für jede abgeschlossene, korrekt und vollständig gelöste Zusatzaufgabe werden entsprechende Notenpunkte erteilt. 

Lernende die eine Zusatzaufgabe bewerten lassen möchten, wenden sich vor Beginn an die Lehrperson. 

Voraussetzung ist, dass bis zu diesem Zeitpunkt alle Laborübungen vollständig abgegeben wurden und jede Laborübung mit voller Punktzahl bewertet wurde. 

# Mögliche Zusatzaufgaben

Ideen für mögliche Zusatzaufgaben:

## Neue Serverdienste in Betriebnehmen

Wählen Sie einen klassischen Serverdienst aus und konfigurieren Sie diesen mit Commandos unter Linux. Greifen Sie anschliessend mit Linux und Windows Clients auf ihren Serverdienst zu. 

Auf Debian ohne GUI installieren und konfigurieren:
 - vsftpd für drei User
 - Extra: ftps !

## DHCP-Snooping / ARP-Snooping

Erarbeiten Sie selbstständig DHCP-Snooping, bauen sie ein Labor in GNS3 auf und zeigen Sie in einem kurzen Video (max. 5 Minuten) wie DHCP-Snooping einen Angriff verhindert. 

Im Video sind ersichtlich:
Grundsätzlich: Wie DHCP-Snooping funktioniert am Beispiel ihres Labors.
 - Die Konfiguration
 - Wie sich Zustände verändert (Status Befehl ausführen: Bevor DHCP-Snooping einen Angriff verhindert hat und danach, gelernte IP Adressen, usw. )
 - Wie im Labor ein Host aktiv versucht selbst DHCP-Server zu sein. 

## Eigener DNS Server in der Cloud

Setze in einem Cloudprovider deiner Wahl (z.B. Microsoft Azure oder Amazon AWS) eine virtuelle Maschine (Debian o.ä.) mit einem Bind9 DNS Server auf. Konfiguriere eine eigene Zone für eine fiktive oder reale Domain. 

**Zusatz:** Werde glücklicher Besitzer (eigentlich Mieter...) einer Domain deiner Wahl. Für bereist 15 CHF/pro Jahr kannst du dir bei [Metanet](https://www.metanet.ch/) oder [GoDaddy](https://www.godaddy.com/) den Traum eines eigenen Domainnamen realisieren. Leite eine Subdomain auf den eigenen Domainserver. 