# Abschlussübung - Modul 123

## Form
In einer Gruppen von **2 bis 4 Personen** ein Mindmap erstellen. 

## Ziel
Ziel ist es die wesentlichen erlernten Handlungskompetenzen festzuhalten und alles nochmals zu rekapitulieren. 

## Lernprodukt
Pro Lernende(r) ein Mindmap auf einem A3 Papier oder Poster

## Inhalte
Das Mindmap sollte Inhalte haben, welche die folgenden Fragen beantworten:
 - Welche Inhalte sind mir vom Modul 123 am meisten geblieben?
 - Welche Übung hat mich am meisten herausgefordert?
 - Kann ich die erlernten Handlungskompetenzen bereits im Betrieb einsetzen? Wenn ja, wie?
 - Was sind die wichtigsten Themengebiete?

## Abschluss
Besprechen Sie ihr Mindmap kurz mit der Lehrperson 
