# DNS Server - PiHole

![DNS GNS3 Beispiel](media/DNS_Ziel.PNG)


*Unser Ziel: Screenshot GNS3 Projekt mit Ubuntu Desktop, Router und DNS-Server Pi-hole*

In dieser Laborübung werden wir:
 - Klassisches DNS beobachten. 
 - PiHole aufsetzen
 - DNSSEC beobachten und mit DNS vergleichen

## Lernziele
 - Kennt die grundsätzlichen Aufgaben eines Servers (Verzeichnisdienst, DHCP, DNS, File, Print) und kann die Vorteile einer zentralen Benutzerverwaltung erläutern.
 - Kennt die Einstellungen zur Konfiguration eines DNS-Servers und kann erläutern, wie diese Einstellgrössen das Verfahren bei der Umsetzung von Namen in IP Adressen (Namensauflösung mittels 'Hosts', DNS) beeinflussen resp. sicherstellen.
 - Kennt die notwendigen Netzwerkeinstellungen eines Clients in einer DHCP-/DNS-Serverumgebung und kann erläutern, welche Auswirkungen diese Einstellungen für den Betrieb eines Hosts/Clients in einem serverbasierten Netz haben.

---

## 1. Vorbereitungen
![DNS GNS3 Beispiel](media/DNS_Ausgangslage.PNG)

*GNS3 Ausgangslage*

 - Auf [mikrotik.com Account anlegen](https://mikrotik.com/client) (wenn nicht bereits vorhanden). Tipp: Account auf TBZ Adresse anlegen. 
 - Neues Projekt anlegen: Datei `DNS Labor.gns3project` in GNS3 importieren
 - *R1* starten
 - Einloggen mit Standard Logindaten: Username: `admin`, Passwort ist keines gesetzt
 - Bei der Aufforderung das Passwort zu ändern: Setzen auf `admin`
 - Updates herunterladen: `/system/package/update/download`
 - Updates installieren `/system/package/update/install` (Router startet anschliessend neu.)
 - System ID zurücksetzen: `/system/license/generate-new-id` (Achtung nicht mehr ausführen nachdem die Lizenz erfolgreich aktiviert wurde!)
 - Konfiguration importieren: `/import default_config.rsc`
 - Ihre Befehlszeile sollte anschliessend so aussehen: 
```
[admin@R1] >
```
 - Lizenz aktiveren (wird benötigt, damit der Router nicht auf 1M drosselt): `/system/license/renew`
 - Account & Passwort des MikroTik Accounts eingeben. Level `p1` wählen. 
 - In der Konfiguration von *PC1* den Befehl `dhcp` hinzufügen.
 - *PC1* starten und Konsole öffnen
 - Testen ob der Zugang ins Internet funktioniert: `ping tbz.ch`

### Fragen
 - Wenn wir beim Ping mit `ping tbz.ch` erfolgreich eine Antwort erhalten, dann können wir gewisse Annahmen über aktive Services treffen. Welche Voraussetzungen sind sehr wahrscheinlich erfüllt (welche Services müssen aktiv sein)?

---

## 2. DNS Query sniffen
![Wireshark](media/Wireshark_DNS.PNG)

*Screenshot: DNS query und DNS query response im Wireshark*

**Ziel:** DNS Query zwischen *PC1* und *R1* mit Wireshark *sniffen* und die relevanten Packete in einer PCAP Datei festhalten
 
### Vorgehen
 - *Packet capture* auf einem Link zwischen *PC1* und *R1* starten
 - *Packet capture* auf einem Link zwischen *R1* und *Internet (nat)* starten
 - *PC1*: Ping auf isc.org
 - In beiden *packet trace* für den DNS Query relevanten Packete identifizieren, markieren und exportieren

### Fragen
 - Welcher Host ist der DNS Server?
 - Wie lange ist die Antwort (*DNS Query Response*) gültig (Time to live)?
 - Was macht der Router mit der erhaltenen DNS Query?
 - Ist die DNS Anfrage signiert? Könnte diese Anfrage gefälscht werden? Begründen.

---

## 3. PiHole aufsetzen
![PiHole](media/Debian_GNS3_Menu.PNG)

*Screenshot GNS3 device menu: Von hier kann eine Debian VM in das Projekt gezogen werden.*

 - Neues Gerät des Typs "Debian 11.x" hinzufügen, mit SW1 verbinden, starten und Konsole öffnen
 - Standard Logindaten: `debian` Passwort: `debian`

**Mehr zur Linux CLI/Shell lernen**
Keine Erfahrungen mit Linux CLI oder Shells? Kein Problem! Im GNS3 Labor können Sie sorgenfrei herumspielen und ausprobieren! Wenn eine Maschine kaputt geht: Kein Problem! Einfach herauslöschen und neu einfügen (Tipp: Führe ein Protokoll mit den ausgeführten Befehlen und Links). 

Weiter stehen im Internet zahlreiche tolle Kurse und Tutorial kostenfrei zur Verfügung:

 - https://missing.csail.mit.edu/
 - https://ryanstutorials.net/linuxtutorial/
 - http://www.ee.surrey.ac.uk/Teaching/Unix/
 - https://miteshshah.github.io/linux/basics/linux-ideas-and-history/
 - https://www.learnenough.com/command-line-tutorial/basics


### Statische IP Adresse konfigurieren
Es gilt als *common practice* dem DNS Server eine statische IP-Adresse zuzuordnen. Der DNS Server ist eine fundamentale Komponente jedes Netzwerkes und soll auch beim Ausfall des DHCP Servers erreichbar sein. 

Weitere Informationen zur Konfiguration von IP-Adressen mit ifupdown: https://wiki.debian.org/NetworkConfiguration

 - `sudo apt autoremove cloud-init` Cloud-Init entfernen, damit dieses nicht mit der IP-Adressen konfiguration interagiert
 - Ins Verzeich der Netzwerkkonfigurationen wechseln `cd /etc/network/interfaces.d/`
 - Von cloud-init erstellte konfiguration wird als Vorlage verwendet. Um Verwechslungen zu vermeiden benennen wir die Datei um: `sudo mv 50-cloud-init default`
 - Datei bearbeiten: `sudo nano default`:
   - ersetzen Sie in der Zeile `iface ensXY inet dhcp` *dhcp* mit `static`
   - Ergänzen Sie nach der Zeile `iface ensXY inet static` anschliessend die statische IP Konfiguration. IP: 192.168.11.10 Maske: 255.255.255.0 Gateway: 192.168.11.1. DNS Nameserver: 192.168.11.1 <br>Die dafür benötiten *statements* finden Sie im oben aufgeführten Link. 
 - Debian neustarten, um die IP Konfiguration anzuwenden. 
 - Testen: Erfolgreiches Anwenden der Konfiguration prüfen und ping test durchführen.


### Pi-hole installieren
Pi-hole Dokumentation: https://docs.pi-hole.net/

 - Installation gemäss https://github.com/pi-hole/pi-hole#one-step-automated-install durchführen.
 - *Hinweise zur Installation:* 
   - Passwort merken (wird am Ende des Installation-Dialoges angezeigt)
   - Alle Installationsparameter auf Standard lassen
 - DNSSEC aktivieren
 - Auf *R1* DNS Nameserver in der DHCP-Server auf Pi-hole IP-Adresse ändern
 - VPC1 neustarten
 - mit VPC1 DNS Anfrage auf isc.org absetzen. 
 - Verifizieren Sie mit geeigneten Mittel folgendes und dokumentieren Sie Ihre Ergebnisse:
   - VPC macht DNS Anfrage über Pi-hole
   - Die weitergeleiteten Pi-hole Anfragen verwenden DNSSEC (Wireshark Dump)
   - Pi-hole loggt/protokolliert DNS Anfragen.

### Korrekter DNS Server im DHCP Server eintragen
Wenn wir im VPCS die IP-Konfiguration prüfen, stellen wir fest, dass der DNS Server immer noch auf den Router zeigt.

*Frage*: Welches Gerät ist im Netzwerk der DHCP-Server? Wo muss der DNS-Server angepasst werden, sodass alle Client Hosts im Netzwerk den richtigen DNS Server ansprechen?

 - Identifizieren Sie den DHCP-Server
 - Loggen Sie sich auf dem entsprechenden Gerät ein. 
 - Passen Sie die Konfiguration an. 
 - Testen Sie die Konifgurationsänderung

*Die notwendigen Schritte sind selbstständig zu recherchieren.*

### Fragen
 - Wie teilt der Client dem Server mit, dass er DNSSEC verwenden möchte?
 - Wie unterscheidet sich eine Query Response mit DNSSEC gegenüber einer ohne?

### Nützliche Debian-Befehle
 - `ip address`: Konfigurierte IP-Adressen pro *interface* anzeigen
 - `ip route`: Konfigurierte Routen 

---

## 4. Pi-hole Ad und Telemetry blockierende Funktionen ausprobieren
Es gibt fast keine Hersteller mehr, die keine Telemetry auf den Benutzergeräten sammeln. Die kommerziellen Betriebssysteme wie Windows, macOS, Android, iOS sammeln fleissig Daten und schicken diese "nach Hause": Angeblich um die "Produkte zu verbessern".
Ob das Fluch oder Segen ist, sei für den Moment dahingestellt. Eines ist jedoch sicher: Die Übermittlung dieser Daten frisst Bandbreite und Energie. 

Eines der Hauptfunktionen von pi-hole ist "Block in-app advertisements" und "Improve network performance". 

Ziel dieses Abschnittes ist das auszuprobieren. 

### Vorgehen
 - UbuntuDesktopGuest einfügen und mit SW1 verbinden
 - Login Passwort: `osboxes.org`, 
 - Beliebige Webseite besuchen (,die sicher Werbung und/oder Tracking enthält)
 - Pi-hole Query Log anschauen. Was für Domains werden geblockt?



# Abschluss

In der Ausgangslage der Laborübung übernahm der Router die *DNS relay* Funktion im LAN. Im laufe der Übung wurde ein zusätzlicher *DNS relay* - Pi-hole eingefügt. Dieser bringt vier Verbesserungen mit sich: 
 - Alle Hosts werden von bekannten gefährlichen DNS Adressen geschützt.
 - Werbung und Tracking wird blockiert, bevor sie heruntergeladen wird. 
 - Pi-hole bietet ein GUI mit Monitoring und Statistik
 - DNSSEC sichert DNS queries vom LAN ausgehend ab. 

