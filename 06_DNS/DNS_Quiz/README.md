# DNS Basics - nslookup

| Praktische Übung  |   |
|---|---|
| Sozialform  |  Einzelarbeit |
| Zeitbudget  |  30 Minuten |
| Lernziele | **B3G:** Ich kann das Grundprinzip der DNS-Namensauflösung erklären.<br>**B3F:** Ich kann das Prinzip und den Ablauf einer DNS-Namensauflösung erläutern und kann Tools einsetzen, um die Funktionalität zu überprüfen.<br>**B3E:** Ich kann die DNS-Auflösung erklären, die Funktionalität überprüfen und über die Hosts-Datei die DNS-Auflösung beeinflussen.  |

---

Mit dieser kleinen spielerischen Übung sollen die wichtigsten Grundlagen zu *DNS record types* und *DNS query tools* erarbeitet werden. 

Nebst IP-Adressen können in *DNS records* auch andere Arten von Daten hinterlegt werden. Diese Arten werden *DNS record types* genannt.

Schauen wir uns einen dieser *DNS record type* an:

<blockquote>
Ein TXT (Text) Record in DNS ist ein Typ von Datensatz, der zusätzliche Textinformationen über einen Server oder Dienst bereitstellt. Diese Informationen können grundsätzlich jede Art von Text sein und für verschiedene Zwecke verwendet werden, wie z.B. zur Überprüfung des Domaineigentums oder zur Verbesserung der E-Mail-Sicherheit durch den Einsatz von SPF (Sender Policy Framework) und DKIM (Domain Keys Identified Mail) Verfahren. Es ist wichtig zu beachten, dass die maximale Länge für einen TXT Record 255 Zeichen beträgt.
</blockquote>
<br>

In dieser praktischen Übung wurde ein DNS Quiz in TXT Einträgen hinterlegt. Führe den ersten DNS Query aus (siehe unten) und folge den Anweisungen. Wenn du alle Schritte erfolgreich meisterst, wirst du über einen TXT Eintrag zum **DNS-Master** ernannt!

Unter Windows kann das Tool `nslookup` verwerdet werden, um *DNS queries* durchzuführen. 

Um mit dem Quiz zu beginnen führe folgenden Befehl in der *Command Prompt* von Windows aus:<br>
`nslookup -q=TXT quiz.alptbz.xyz`

In der *DNS query response* findest du die erste Anweisung. 

![nslookup](media/nslookup.PNG)

Das Argument `-q=TXT` weisst nslookup an einen TXT query durchzuführen. Der Wert `TXT` kann durch einen beliebigen *DNS record type* ersetzt werden.

Viel Erfolg!

## Hinweis für MacOS und Linux Benutzer

Unter MacOS und Linux steht das mächtige Tool `dig` zur Verfügung. Der Befehl zum Starten des Quiz sieht wie folgt aus:

`dig quiz.alptbz.xyz TXT`

oder 

`dig -t TXT quiz.alptbz.xyz`

## Hinweis DNS Server
Es kann sein, dass der deinem PC bekannten DNS Server Einträge filtert und blockiert. Du kannst `nslookup` und `dig` den zu verwendenden *DNS Server* als Argument mitgeben. Beispiele:

Windows:

`nslookup -q=TXT quiz.alptbz.xyz 9.9.9.9`

MacOS / Linux:

`dig @9.9.9.9 -t TXT quiz.alptbz.xyz`