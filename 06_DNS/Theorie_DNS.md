# Grundlagen DNS

## Lernziele
 - B3G: Ich kann das Grundprinzip der DNS-Namensauflösung erklären.
 - B3F: Ich kann das Prinzip und den Ablauf einer DNS-Namensauflösung erläutern und kann Tools einsetzen, um die Funktionalität zu überprüfen.
 - B3E: Ich kann die DNS-Auflösung erklären, die Funktionalität überprüfen und über die Hosts-Datei die DNS-Auflösung beeinflussen.

## Einführung
Es ist aus der bisherigen Diskussion ersichtlich, dass jede Maschine in einem Netzwerk durch ihre IP-Adresse identifiziert und adressiert wird. Dennoch stellte man früh fest, dass dieses numerische System für den Benutzer nicht besonders freundlich ist. Daher wurde ein Prozess zur Auflösung von Namen implementiert, der auf einer Textdatei basiert. Diese wurde hosts.txt genannt und später nur noch "hosts". Diese Datei beinhaltet eine Tabelle, die jedem Hostnamen eine entsprechende IP-Adresse zuweist.
Jeder Systemadministrator musste seine hosts-Dateien selber pflegen. Jedes Mal wenn ein Rechner neu dazu kam, musste eine neue Zeile eingetragen werden. Beim Ändern einer Adresse musste diese auch in allen hosts-Dateien geändert werden. Das Ganze wurde bald unübersichtlich, so dass unterschiedliche Versionen der hosts-Dateien herumgeisterten.
Aus diesem Grund folgte die Gründung von «Internic», einer Universitätsstelle die die hosts-Dateien verwaltete. Administratoren meldeten Änderungen dorthin und bezogen aktuelle Versionen von dort. Mit dem weiteren Anwachsen des Rechnerbestands war auch dieser Internic-Server bald einmal überlastet. Als Folge davon wurde das DNS-System entwickelt. Ein System von Namenservern, das nur bei Bedarf den benötigten Datensatz an den anfragenden Rechner sendet.

## hosts

Die hosts-Datei ist eine Textdatei, die auf dem System lokal gespeichert ist und eine Verbindung zwischen der IP-Adresse und dem Hostnamen herstellt. Diese Datei existiert unabhängig vom Betriebssystem und ist bei Unix/Linux-Systemen in /etc/hosts und bei Windows-Systemen in %Systemroot%\system32\drivers\etc zu finden.

Die Struktur der Datei ist typischerweise wie folgt:

```
127.0.0.1     localhost
192.168.1.10  dateiserver
130.92.250.18  uni-bern
```

Zusätzlich zu den Verteilungsschwierigkeiten wurde die hosts-Datei auch oft von Schadprogrammen genutzt, um den Aufruf von Internetseiten zu manipulieren.

Die hosts-Datei wurde zu einer Zeit entwickelt, als alle Computer im Internet öffentlich zugänglich waren. Mit der zunehmenden Verbreitung von privaten Netzwerken, die auf TCP/IP basieren, ergab sich das Problem, dass diese Netzwerke nicht mehr automatisch via FTP aktualisiert werden konnten. Diese gesamte Problemlage führte zur Einführung eines neuen Systems, dem sogenannten Domain Name System (DNS), und auch zu einer zeitweiligen, proprietären Lösung von Microsoft namens WINS. Trotzdem haben heutige Client-Rechner immer noch eine hosts-Datei. Gerade wenn es um die Gefahr durch Schadsoftware geht, kann es hilfreich sein, diese Datei im Falle von Problemen zu überprüfen.

## DNS (Domain Name System)

Erstmals beschrieben wurde das Domain Name System (DNS) im Jahr 1983 und seit 1987 ist es in den RFC 1034 und RFC 1035 festgelegt. 
Der Hauptzweck dieses DNS-Systems liegt in der Herstellung von Struktur und Ordnung. 

Die Ziele:
 - Eine schnelle und automatische Aktualisierung der Namenslisten ist vorhanden, um sicherzustellen, dass die Informationen allen Netzwerkteilnehmern zugänglich sind.
 - einheitlicher Namensraum , der unabhängig von IP-Elementen und allen Netzwerkknoten
 - Lokale Caches, die zur Beschleunigung der Anfragen beitragen.
 - Der Namensraum ist nicht nur auf bestimmte Dienste oder Protokolle wie beispielsweise "www" oder "http" beschränkt, sondern kann auch für andere Dienste und Protokolle verwendet werden.
 - Die Forward-Lookup-Zone ermöglicht die Umwandlung eines Namens in eine IP-Adresse.
 - Die Reverse-Lookup-Zone ermöglicht die Umwandlung einer IP-Adresse in einen Namen.
 - Jeder Nameserver ist in der Lage, nur die Namen und Adressen für die Zone zu entschlüsseln, für die er verantwortlich ist. Falls er die Auflösung nicht durchführen kann, fordert er den nächsten verantwortlichen Server an.

## DNS record types
| Typ   | Beschreibung                                                                                                                                                                                                                        |
|-------|-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| A     | Host-Adresse IPv4: Wird verwendet, um einen Domainnamen in eine IPv4-Adresse (32 Bit) umzuwandeln.                                                                                                                                  |
| AAAA  | Host-Adresse IPv6: Wird verwendet, um einen Domainnamen in eine IPv6-Adresse (128 Bit) umzuwandeln.                                                                                                                                 |
| CNAME | Alias-Name: Wird verwendet, um einen Domainnamen auf einen anderen Domainnamen zu verweisen. Es ist hilfreich, wenn man denselben Standort mit verschiedenen Namen erreichen möchte.                                                |
| MX    | Mail-Server-Name: Zeigt auf den Server, der E-Mails für diese Domain verarbeitet.                                                                                                                                                   |
| NS    | Authoritativer Name Server für diese Domain: Delegiert eine DNS-Zone an einen bestimmten Server, der die vollständige Autorität für die Subdomains dieser Zone hat.                                                                 |
| PTR   | Pointer, Domain Name Pointer: Wird hauptsächlich für Reverse-DNS-Lookups verwendet. Es verknüpft eine IP-Adresse mit einem Domainnamen.                                                                                             |
| SOA   | Identifiziert den Startpunkt der Zone: Das SOA-Record enthält Informationen über die grundlegenden Eigenschaften der DNS-Zone, wie den Namen des Servers, der die Zone verwaltet, und andere globale Parameter.                     |
| TXT   | Text: Wird oft für verschiedene Zwecke verwendet, einschließlich der Überprüfung des Domainbesitzes, des SPF-Records (Sender Policy Framework) zur Bekämpfung von E-Mail-Spoofing und zur Bereitstellung allgemeiner Informationen. |

<br>

---

## Die Hierarchische Struktur des DNS
![Hierarchische Struktur DNS](media/HierarchischeStruktur.png)

Die DNS-Domäne bildet einen eigenständigen Namensraum (Namespace), der durch den Inhalt der DNS-Datenbank begrenzt wird, die allgemein als Zonendatei bezeichnet wird. In diesem System ist der vollqualifizierte Domänenname (Fully Qualified Domain Name, FQDN) streng hierarchisch organisiert und wird von rechts nach links gelesen.

Die oberste Ebene einer jeden Hierarchie ist die Stammdomäne oder Root-Domäne, gekennzeichnet durch eine Nullbezeichnung, symbolisiert durch ein abschließendes Punktzeichen «.». Unmittelbar unter dieser Stammdomäne befinden sich die Top-Level-Domains (TLDs). Diese können entweder generisch (z.B. .com, .org, .net) oder länderspezifisch (z.B. .de für Deutschland, .ch für die Schweiz) sein, je nach Organisationstyp oder Land, welches sie repräsentieren.

Auf der darauffolgenden Ebene, den Second-Level-Domains, können sowohl individuelle Hosts als auch Subdomains existieren. Ein Beispiel für eine Second-Level-Domain wäre der Name einer Firma in einer URL wie 'firma.ch'. Weitere Subdomains (Third-Level, Fourth-Level usw.) liegen unterhalb der Second-Level-Domains und dienen der weiteren Strukturierung und Organisation innerhalb des DNS-Hierarchiesystems.

## FQDN (Fully Qualified Domain Name)
![FQDN Example](media/AufbauDNS1.PNG)

## Ablauf DNS Query
In der nachfolgenden Grafik wird ein typischer DNS Query abgebildet. 
Der nachfolgende Ablauf findet nur in dieser Form statt, wenn weder der *DNS Forwarder* noch der *DNS resolver* die gewünschte Domain im Cache gespeichert haben. 

![DNS Query](media/DNSQuery.PNG)

Beispiel Szenario: Der User gibt die Domain "tbz.ch" in der Adresszeile des Browsers ein. Das Betriebssystem hat diese FQDN nicht im Cache und führt die nachfolgende Anfrage durch. 

1. Der Laptop schickt ein DNS Query an den DNS Forwarder.
2. Der *DNS Forwarder* (typsicherweise der "Router" bzw. Gateway in Internet) hat die gewünschte FQDN nicht im Cache und schickt die Anfrage (= *DNS query*) an den Konfigurierten *DNS Resolver* weiter. 
3. Der *DNS Resolver* prüft ebenfalls zuerst seinen Cache. Da der die FQDN nicht finden kann, schickt er die Anfrage an einen *Root Server* weiter.
4. Der *Root Server* antwortet mit einer *query response* bzw. *dns answer*. Diese beinhaltet den *Authoritativer Name Server für diese Top-Level-Domain* (NS). 
5. Der *DNS resolver* schickt die *DNS query* an den Top-Level-Domain Server. 
6. Der *TLD server* antwortet mit dem *Name server* für die entsprechende Domain.
7. Der *DNS resolver* schickt die *DNS query* an den Nameserver der eigentliche Domain. 
8. Der Nameserver antwortet nun mit dem eigentlichen Eintrag für die FQDN. 
9. Der *DNS resolver* schickt die Antwort weiter an den *DNS Forwarder*. 
10. Der *DNS forwader* schickt die Antwort zurück an den Laptop. 

## Tools
![DNS Tools](media/DNSTools.PNG)
 - Windows: nslookup
 - Linux: dig<br>Dieses Tool ist meistens nicht Teil der Standardpakete und muss mit dem entsprechenden Paketmanager nachinstalliert werden. 

### Dig
![DNS Tool Dig](media/DNSToolDig.PNG)

## Wie registriere ich eine Domain?
![DNS TLDs](media/TLDs.png)
<br>*Abbildung: Aktuell sind 1479 TLDs bei IANA (Internet Assigned Numbers Authority) aufgeführt. Quelle: https://www.icann.org/resources/pages/tlds-2012-02-25-en*

Die [*Top-Level-Domains* (TLD)](https://en.wikipedia.org/wiki/Top-level_domain) wie `.ch`, `.com` usw. werden von [IANA](https://www.iana.org/) verwaltet. Die IANA ist für die Verwaltung der DNS root zone verantwontworlich. Organisationen können sich bei der IANA für die Verwaltung einer *TLD* bewerben. Dafür muss die Organisation einige Voraussetzungen erfüllen. 

Wenn eine Firma, Organisation oder Privatperson eine Domain (z.B. `tbz.ch`) möchte, dann "mietet" sie diese bei einem [Registrar](https://en.wikipedia.org/wiki/Domain_name_registrar). Häufig erfolgt die "Miete" der Domain nicht direkt über den Registrar, sondern über Zwischenhändler wie [Metanet](https://www.metanet.ch/), [GoDaddy](https://www.godaddy.com/de-ch), [Swizzonic](https://www.swizzonic.ch/), [Hostpoint](https://www.hostpoint.ch/en/), usw. Diese Dienstleister bieten häufig weitere *Hosting* Dienstleistungen an. Das beinhaltet häufig Webseiten/-Applikationen und E-Mail Hosting. Es ist jedoch meist auch möglich nur die Domain zu mieten, ohne die Hosting-Dienstleistungen.  

![FQDN](media/FQDN.PNG)
<br>*Abbildung: Aufbau einer Domain.*

Nur die *Domain* muss beim Registrar gemietet werden. Alle anderen Teile kann der DNS-Domain-Mieter selbst bestimmen. Die Subdomain, der Hosts usw. können selbst definiert werden. 

Vorgehen eigene Domain mieten:
 - 1. **Zwischenhändler auswählen:** Suchen sie sich einen Zwischenhändler ihrer Wahl aus. Tipp: Bevorzugen Sie Zwischenhändler im eigenen Land oder grössere Zwischenhändler. 
 - 2. **Freie und zahlbare Domain finden:** Meist ist die gewünschte Domain nicht mehr erhältlich oder kostet sehr viel. Es braucht ein wenig Zeit bis die gewünschte Domain gefunden wurde. 
 - 3. **Domain registrieren**: Beim Zwischenhändler kann die gewünschte Domain für eine Laufzeit meistens von 1 bis 5 Jahren gemietet werden. Meist wird dafür eine Kreditkarte benötigt und ein Account muss angelegt werden.
 - 4. **Nameserver konfigurieren**: Die meisten Zwischenhändler bietet zur Domain einen kostenlosen Nameserver für ihre Domain an. Dort können die gewünschen *DNS records* gleich hinterlegt werden. Falls ein anderer Nameserver verwendet werden soll, kann dieser hinterlegt werden.
 - 5. **Dienste bereitstellen**: Sei es ein E-Mail, Gaming, Webserver, Online-Shop: Nun können Dienste bereitgestellt werden, welche mithilfe ihrer Domain erreichbar sind. 

## Fragen
 - Wo befindet sich die Zonendatei (Datei mit DNS-Einträgen) für die Domain "tbz.ch", die vom Domaininhaber verwaltet wird?
 - Was ist der Unterschied zwischen einem *DNS forwarder* und einem *DNS resolver*?
 - Ist der DNS Service eines typischen Home-Routers (z.B. [Swisscom Internet Box](https://www.swisscom.ch/de/privatkunden/produkte/internetrouter/details.html/internet-box-4-fibre-11050518?stch=plp&ing=ptl-device-first&simplyDigital=true&journey=device-first&contractDuration=24&useCase=HARDWAREONLY&payOption=ONE_TIME)) ein *DNS forwarder* oder *DNS resolver*?
 - Welcher Teil der FQDN (TLD, Second-Level, Third-Level, usw. ) wird bei einem [*Domain Name Registrar*](https://en.wikipedia.org/wiki/Domain_name_registrar) definiert und welche Teile können durch den Besitzer frei definiert werden?

# Quellen
 - https://en.wikipedia.org/wiki/Domain_name_registrar
 - https://en.wikipedia.org/wiki/Domain_name

