# DNS Basics - DNS Query Trace

| Praktische Übung  |   |
|---|---|
| Sozialform  |  Einzelarbeit |
| Zeitbudget  |  30 Minuten |
| Lernziele | **B3G:** Ich kann das Grundprinzip der DNS-Namensauflösung erklären.<br>**B3F:** Ich kann das Prinzip und den Ablauf einer DNS-Namensauflösung erläutern und kann Tools einsetzen, um die Funktionalität zu überprüfen.<br>**B3E:** Ich kann die DNS-Auflösung erklären, die Funktionalität überprüfen und über die Hosts-Datei die DNS-Auflösung beeinflussen.  |

---

# Übung 1 - DNS Query Ablauf

![Ablauf DNS Query](media/AblaufDNSQuery.PNG)
*Abbildung 1: Ablauf eines DNS Queries, wenn der Resolver und Forwarder für den jeweiligen Query nichts im Cache gespeichert haben.*

## Input

In der DNS Infrastruktur gibt es unterschiedliche DNS Services, welche miteinander zusammenarbeiten:
 - **DNS Forwarder:** Typischerweise auf Home-Routern und Small Business Routern zu finden (Router in diese Fall = "Kombigerät"). Kann auch durch einem Server (z.B. Active Directory Server). Schickt alle Anfragen, welche er nicht im Speicher hat an den einprogrammierten DNS-Resolver weiter. 
 - **DNS Resolver:** Häufig bei ISP zu finden, welche einen DNS Resolver für die Endkunden bereitstellen. Ein DNS Resolver kann auch auf eigener Infrastruktur betrieben werden. Löst DNS Anfragen auf, indem er der "Hierachie-nach" auflöst.
 - **Root Server:** [siehe Root-Nameserver](https://de.wikipedia.org/wiki/Root-Nameserver):  Die Root-Zone umfasst Namen und IP-Adressen der Nameserver aller Top-Level-Domains (TLD).
 - **TLD Server:** Zentrale Informationsquelle für die Abfrage von Nameserver-Daten zu Domains.
 - **Nameserver:** Stellt die Zone für die gewünschte Domain bereit. 

## Übung

Ordne die nachfolgenden Aussagen den Zahlen in der *Abbildung 1* zu. Deine Lösung kannst du mithilfe dem Abschnitt [Theorie DNS](../Theorie_DNS.md) überprüfen. 

 - Der *DNS resolver* schickt die *DNS query* an den Top-Level-Domain Server. 
 - Der *DNS resolver* schickt die *DNS query* an den Nameserver der eigentliche Domain. 
 - Der *DNS Resolver* prüft ebenfalls zuerst seinen Cache. Da der die FQDN nicht finden kann, schickt er die Anfrage an einen *Root Server* weiter.
 - Der Laptop schickt ein DNS Query an den DNS Forwarder.<br> Bsp: `DNS query: Class: IN, Type: A, Domain: www.tbz.ch`
 - Der *DNS forwarder* schickt die Antwort zurück an den Laptop. 
 - Der *DNS Forwarder* (typsicherweise der "Router" bzw. Gateway in Internet) hat die gewünschte FQDN nicht im Cache und schickt die Anfrage (= *DNS query*) an den Konfigurierten *DNS Resolver* weiter. 
 - Der *DNS resolver* schickt die Antwort weiter an den *DNS Forwarder*. 
 - Der *TLD server* antwortet mit dem *Name server* für die entsprechende Domain.
 - Der *Root Server* antwortet mit einer *query response* bzw. *dns answer*. Diese beinhaltet den *Authoritativer Name Server für diese Top-Level-Domain* (NS). 
 - Der Nameserver antwortet nun mit dem eigentlichen Eintrag für die FQDN. 

# DIG - DNS lookup utility
[DIG (domain information groper)](https://linux.die.net/man/1/dig) ist ein flexibles Tool, um mit DNS Name-Servern zu kommunizieren. Unter Linux und MacOS kann es mithilfe von Paketverwaltungsprogrammen einfach installiert werden. Unter Windows kann DIG mit [Chocolatey](https://chocolatey.org/) oder [Cygwin](https://cygwin.com/) installiert werden (Voraussetzung: Entsprechendes Tool wird zuerst installiert).

Zu Test und Demozwecken kann auf auf Web-Services zurückgegriffen werden, wie [www.digwebinterface.com](https://www.digwebinterface.com/) zurückgegriffen werden. 

Dig erlaubt eine vielzahl von Queries. Unter anderen auch einen "Trace". In diesem Modus löst, wie ein DNS-Resolver, DIG den Query mithilfe der Root-Server auf. 

Im nachfolgenden Beispiel wird der Output eines DIG Traces in die einzelnen Phasen aufgeteilt und erklärt.  

**Die Root-Server und deren IP-Adressen müssen dem Resolver zuvor bekannt sein. Die IPv4-Adressen der Resolver sind öffentlich bekannt und ändern sich sehr selten**
```
# Liste der bekannten ROOT Servern:
.			18537	IN	NS	e.root-servers.net.
.			18537	IN	NS	h.root-servers.net.
.			18537	IN	NS	l.root-servers.net.
.			18537	IN	NS	i.root-servers.net.
.			18537	IN	NS	a.root-servers.net.
.			18537	IN	NS	d.root-servers.net.
.			18537	IN	NS	c.root-servers.net.
.			18537	IN	NS	b.root-servers.net.
.			18537	IN	NS	j.root-servers.net.
.			18537	IN	NS	k.root-servers.net.
.			18537	IN	NS	g.root-servers.net.
.			18537	IN	NS	m.root-servers.net.
.			18537	IN	NS	f.root-servers.net.
```

**(Schritt 3 und 4): Anfrage an den Root Server. Dieser gibt Nameservern für die TDL .ch zurück:**
```
;; Received 525 bytes from 8.8.8.8#53(8.8.8.8) in 11 ms

ch.			172800	IN	NS	f.nic.ch.
ch.			172800	IN	NS	a.nic.ch.
ch.			172800	IN	NS	e.nic.ch.
ch.			172800	IN	NS	b.nic.ch.
ch.			172800	IN	NS	d.nic.ch.
```

**(Schritt 5 und 6): Anfrage an einen der fünf TLD Server, gibt Name-Server der gewünschten Domain zurück:**
```
;; Received 676 bytes from 202.12.27.33#53(m.root-servers.net) in 46 ms

tbz.ch.			3600	IN	NS	dns1.swizzonic.ch.
tbz.ch.			3600	IN	NS	dns2.swizzonic.ch.
```

**(Schritt 7 und 8):Anfrage an den Name-Server der Domain gibt, gewünschten Eintrag zurück:**
```
;; Received 229 bytes from 194.0.25.39#53(d.nic.ch) in 1 ms

tbz.ch.			900	IN	A	149.126.4.25
```
## Übung
 - Finde mithilfe von DIG Trace heraus, wie die Domain(s) der TLD-Name-Servern einer ".com" oder ."org" Domain lauten.
 - Welche Organisationen (Firmen) beitreiben die ".com" oder die ".org" Domain?

# Verständnisfragen:
 - Auf welchem der drei Nameservern (Root, TLD, Nameserver der Domain) hinterlegt der DNS Administrator von tbz.ch einen `CNAME` Eintrag?
 - Auf welchem der drei Nameservern (Root, TLD, Nameserver der Domain) muss ein DNS Administrator den Nameserver der Domain hinterlegen?
 - Aus welchem Grund kann für ein DNS Query mehrere Responses zurückkommen?
 - Wenn mehr als ein Nameserver Eintrag als Antwort zurückkommt: Welcher Eintrag ist gültig? (Im Beispiel: dns1.swizzonic.ch oder dns2.swizzonic.ch ?) und weshalb?


