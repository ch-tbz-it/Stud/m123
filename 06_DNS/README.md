# DNS

Das *Domain Name System* (DNS) erlaubt es dem Benutzer Namen zu verwenden um Computer zu referenzieren bzw. dazugehörige IP-Adressen zu finden. DNS verwendet ein Client/Server Modell, während DNS server vom IT Personal verwaltet wird. Die DNS Client Funktionalität ist heutzutage ein Teil eines jedes Gerätes, welches TCP/IP verwendet. Die Funktionsweise von DNS ist simpel: Der Client fragt den Server nach der korrepondierenden IP-Adresse für eine bestimmte Domain. 

DNS ist eines der grössten Angriffsziele für Netzwerkangriffe. Als eines der ältesten und wichtigsten Protokollen im modernen Internet, ist DNS die Fundament für viele Services und Protokolle. Das macht DNS ein attraktives Ziel für Angreifer. 

# Inhalt
 - Theoretische Grundlagen erarbeiten
   - Wie funktioniert DNS?
   - Gefahren und Angriffe auf DNS
   - Laborübungen: DNS absichern mit PiHole

# Theoretische Grundlagen


Als Vorbereitung für die Laborübung:

1. Einlesen in die [Grundlagen DNS](Theorie_DNS.md) und Fragen beantworten. 
2. Schauen Sie sich eines der vielen Erklär-Videos zum Thema finden Sie auf [Youtube](https://www.youtube.com/results?search_query=dns). 

Zusätzliche Ressourcen für Fortgeschrittene:
 - Ausschnitt `01_DNS Einführung.pdf` lesen. Eine kurze Einführung in das Thema DNS. 
 - Ausschnitt `02_Threads to DNS Security.pdf` lesen.
 - DNS Sicherheit ich diesen Vortrag von Dan Kaminsky:
[DEF CON 16 - Dan Kaminsky: Black Ops 2008](https://www.youtube.com/watch?v=7Pp72gUYx00) anschauen.

Die PDFs stehen in der Dokumentenablage zum Modul (Meistens in der MS Teams Gruppe unter Allgemein -> Dateien/Dokumente -> Kursmaterialien) zur Verfügung.

# Laborübungen mit Packet Tracer
Die nachfolgenden Laborübungen bauen aufeinander auf. Denken Sie daran die Laborübungen gemäss Anweisungen in [02_Lernjournal](../02_Lernjournal) zu dokumentieren (Beachte Abweichende Anweisungen des Kursleiters/Lehrperson).

## Praktische Übung 1: DNS Quiz
[Auf zur praktischen Übung](DNS_Quiz/README.md)

## Laborübung 1: DNS Tracing
[Auf zum Labor 1 (Packet Tracer)](DNS_PacketTracer/02_DNS.md)


# Laborübungen mit GNS3 (Fortgeschritten)
Die nachfolgenden Laborübungen bauen aufeinander auf. Denken Sie daran die Laborübungen gemäss Anweisungen in [02_Lernjournal](../02_Lernjournal) zu dokumentieren, sofern vom Kursleiter / Lehrperson gefordert.

## Laborübung 1: Pi-hole: Ein DNS Server und "Müll"-Filter
[Auf zum Labor 1 (GNS3) ](DNS_GNS3/01_DNS%20Server.md)