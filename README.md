# Modul 123 - Serverdienste in Betrieb nehmen

# Kompetenzen
Siehe [Kompetenzmatrix](./00_Kompetenzmatrix/README.md)

# Modulbeschreibung
Siehe [modulbaukasten.ch](https://www.modulbaukasten.ch/module/123/)

# Aufbau
Ziel ist es die Kompetenzen gemäss [Kompetenzmatrix](./00_Kompetenzmatrix/README.md) zu erreichen. Die nachfolgende Reihenfolge der Inhalte ist eine Empfehlung. 


| N° | Titel                                                | Kompetenzband   | Kompetenz |
|----|------------------------------------------------------|-----------------|-----------|
| 1  | [Einführung Serverdienste](./03_Grundlagen/01_Einfuehrung/README.md)  | Grundlagen      | B1G, K1G |
| 2  | [Repetition Modul 117 – Teil 1](./01_Repetition/1_Repetition%20117%20Quiz.md) | Grundlagen      |  | 
| 3  | [Installation Cisco Packet Tracer](04_Cisco%20Packet%20Tracer/README.md)  | Grundlagen  | F1G |
| 4  | [Repetition M117 - Fehlersuche im Netzwerk](./01_Repetition/2_Fehlersuche%20im%20Netzwerk/README.md)         | Grundlagen      |  C2G, J1G  |
| 5  | [Grundlagen Serverdienste](./03_Grundlagen/README.md) | Grundlagen   | B1G, B1F |
| 6  | [Serverdienste beim “Surfen”](03_Grundlagen/docs/Serverdienste%20beim%20Surfen.pdf) | Grundlagen      | B1G, B1F | 
| 7  | [Serverhardware definieren](./03_Grundlagen/02_Serverhardware/README.md)     | Grundlagen      | A1G, A1F |
| 8  | [DHCP Grundlagen](05_DHCP/README.md)              | Grundlagen      | B2G |
| 9  | [DHCP Labor](05_DHCP/DHCP_PacketTracer/01_DHCP.md)  | Grundlagen   | B2G , B2F, B2E |
| 10 | [DNS Grundlagen](06_DNS/README.md) | Grundlagen      | B3G | 
| 11 | [DNS Query Quiz](06_DNS/DNS_Quiz/README.md)     | Grundlagen      | B3G, B3F | 
| 12 | [DNS Labor](06_DNS/DNS_PacketTracer/01_DNS.md)   | Grundlagen      | B3G, B3F | 
| 13 | [Grundlagen Dateifreigabe](./09_Dateifreigabe/README.md) | Grundlagen | B4G | 
| 14 | [Grundlagen Zugriffsschutz](./09_Dateifreigabe/README.md) | Grundlagen | G1G | 
| 15 | [Grundlagen Drucken](./11_Drucken/README.md) | Grundlagen | B5G | 
| 16 | [HTTP Grundlagen](07_HTTP/README.md)            | Grundlagen      | B1G, B1F |
| 17 | [HTTP Webserver Übung](07_HTTP/HTTP_Webserver/README.md)    | Grundlagen      |  B1G, B1F, D1G, D1F, E1G, E1F, I1G, I1F |
| 18 | [Einführung GNS3](./04_GNS3%20Einführung/README.md) | Fortgeschritten | F1G, K1G, K1F | 
| 19 | [Fortgeschritten 01: GNS3 Labor - DHCP mit Cisco](05_DHCP/DHCP_GNS3/01_DHCP%20Server%20Konfiguration%20Cisco.md)    | Fortgeschritten |  K1G, K1F, B2G, B2F, B2E |
| 20 | [Fortgeschritten 02: GNS3 Labor - DHCP mit MikroTik](05_DHCP/DHCP_GNS3/02_DHCP%20Server%20auf%20MikroTik%20Router.md) | Fortgeschritten |  K1G, K1F, B2G, B2F, B2E |
| 21 | [Fortgeschritten 03: GNS3 Labor – DNS](06_DNS/DNS_GNS3/01_DNS%20Server.md)  | Fortgeschritten | K1G, K1F, B3G, B3F, B3E | 
| 22 | [Fortgeschritten 04: GNS3 Labor – TFTP](08_Dateiübertragung/01_TFTP.md) | Fortgeschritten | K1G, K1F, B1F |
| 23 | [Fortgeschritten 05: GNS3 Labor - HTTP, Wireshark](07_HTTP/HTTP_GNS3/01_README.md)  | Fortgeschritten |  K1G, K1F, B1G, B1F |
| 24 | [Experte: GNS3 Labor – NAS](08_Dateiübertragung/02_NAS.md)     | Erweitert       | C1G, C1F, C2G, C2F, D1G, D1F, E1G, E1F, F1G, F1F, G1G, G1F |


# Lizenz
<a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/">Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License</a>.