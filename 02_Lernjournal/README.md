# Lernjournal - Dokumentation mit Git und Markdown

**Wichtig: Die nachfolgenden Anweisungen sind für die Modulbewertung relevant. Es wird empfohlen dieses Dokument genau zu lesen und Unklarheiten mit der Lehrperson zu klären.**

**Tipp: Schauen Sie sich das Beispiel Repository an (siehe Link unten im Abschnitt Videoanleitung)**

In diesem Modul werden verschiedene Laborübungen bearbeitet. Alle Laborübungen müssen im persönlichen Lernjournal (Repository) mit Markdown dokumentiert werden. Dieses Lernjournal ist im Markdown Syntax zu schreiben und in einem persönlichen Git Repository zu speichern. 

 - Für jedes Labor ist im Lernjournal ein Laborbericht anzulegen. 
 - Ein Laborbericht ist eine einzelne Markdown Datei.
 - Alle Bilder, Videos usw. sind in einem separaten Unterverzeichnis abzulegen (z.B. `media` oder `images`).
 - Für jedes Labor einen eigenen Ordner anlegen. 

# Wichtig
 - **Keine Bedienungsanleitungen schreiben!** Dinge wie "GNS3 Projekt importieren" gehören nicht in ihr Lernjournal. 
 - **Relevant** sind: Getätigte Konfiguration (ohne Standardkonfiguration, nur Delta!), Aufbau, Screenshot des Labors, Beschreibung des Laborsetups, Links zu der Aufgabenstellung, verwendete Hilfestellungen (z.B. Link), usw.
 - **Richtiges Dateiformat** Alle *Wireshark Dumps* sind als *PCAP*-Datei abzugeben. *PCAPNG*-Dateien sind **NICHT** zulässig. 

# Allgemeine Informationen

[Markdown](https://de.wikipedia.org/wiki/Markdown) ist die führende Aufzeichnungssprache, wenn es um die Systemdokumentation geht. Der Syntax ist einfach und intuitiv. 

Markdown-Dateien werden automatisch von GitLab gerendert. 

Im Netz sind verschiedene Markdown Editoren. Es wird grundsätzlich zwischen zwei verschiedenen Arten unterschieden: 
 - [WYSIWYG](https://de.wikipedia.org/wiki/WYSIWYG) Editoren
 - Code Editoren mit [*Syntax Highlighting*](https://de.wikipedia.org/wiki/Syntaxhervorhebung)

Markdown ist ein textbasiertes Dateiformat und ist deshalb für den Einsatz mit Git geeignet. 

In den Unterlagen des Moduls 231 finden Sie eine [Einführung zu Git und Markdown](https://gitlab.com/ch-tbz-it/Stud/m231/-/tree/master/10_Git).

# Videoanleitung
**Achtung: Diese Anleitung wurde für das Modul 129 erstellt. Folgende Abweichungen gilt es zu beachten:**
 - Ihre Modulnummer ist *123* NICHT 129!
 - Die gezeigte zu übernehmende Dokumentationsvorlage ist für das Modul 129 und nicht für das Modul 123. 
 - Eine Vorlage für die Dokumentation von Netzwerklabore finden Sie hier: https://gitlab.com/alptbz/beispiel-lernjournal-fuer-laboruebungen
 - Wenn bereits ein GitLab Account vorhanden ist, muss kein Neuer angelegt werden. 


In dem nachfolgenden Video wird gezeigt, wie Sie einen...
- GitLab Account anlegen, 
- ein GitLab Projekt erstellen,
- Visual Studio Code und Git installieren,
- ein SSH Schlüsselpaar erstellen und den Public-Key ihrem GitLab Account hinzufügen,
- zusätzliche Markdown Plugins in VS Code installieren,
- das Template in ihrem Projekt einfügen,
- den ersten Commit erstellen und pushen,
- E-Mail und Benutzername in der Git Konfiguration für Ihren lokalen Benutzeraccount festlegen.

![Video zu VS Code Git Markdown GitLab](https://gitlab.com/ch-tbz-it/Stud/m129/-/raw/main/04_MarkdownGit/videos/CodeGitMarkdown.webm)

**Achtung! Verwenden Sie dieses Template hier: https://gitlab.com/alptbz/beispiel-lernjournal-fuer-laboruebungen und nicht das von 129**
