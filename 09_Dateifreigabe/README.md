# Dateifreigabe

Das Prinzip der Netzwerkfreigabe dient der zentralen Verwaltung und Speicherung von Dateien, was das Backup vereinfacht und gemeinsames Arbeiten ermöglicht. Durch die Freigabe von Daten auf einem Server können autorisierte Nutzer von verschiedenen Geräten innerhalb eines Netzwerks aus auf dieselben Dateien zugreifen, sie lesen oder bearbeiten, ohne sie lokal speichern zu müssen.

## Netzwerkfreigaben unter Windows

In Windows-Systemen werden Netzwerkfreigaben über das SMB-Protokoll (Server Message Block) bereitgestellt. Dieses Protokoll ermöglicht die zentrale Verwaltung und Speicherung von Dateien und vereinfacht das Backup sowie das gemeinsame Arbeiten. Durch die Verwendung des SMB-Protokolls können Administratoren mithilfe der Active Directory-Benutzerverwaltung oder einer Berechtigungsmatrix den Zugriff auf freigegebene Ressourcen steuern. SMB funktioniert verbindungsorientiert und bietet Sicherheitsoptionen wie Verschlüsselung und Sitzungssteuerung. Auf eine Windows-Freigabe wird durch Pfadangaben im Format `\\HOST\SHARE` zugegriffen, wobei `HOST` den Server und `SHARE` den Namen der freigegebenen Ressource bezeichnet.

### Wichtigste Themengebiete Windows
 - **Dateidienste mit Windows:** Eine gut geplante Architektur ist entscheidend für die reibungslose Datenbereitstellung. Dazu gehören klare Daten-, Namens-, Adressierungs- und Netzwerkkonzepte, die sicherstellen, dass jeder Benutzer die Daten finden und darauf zugreifen kann, die er benötigt.
 - **Datenkonzept:** Die Verwaltung von Zugriffsrechten auf Verzeichnisse muss detailliert geplant sein, um sicherzustellen, dass sensible Daten nur von autorisierten Benutzern gelesen oder verändert werden können. Datenschutz und Vertraulichkeit stehen im Vordergrund und sind Grundvoraussetzungen für eine sichere Datenfreigabe.
 - **Berechtigungsmatrix und Gruppen:** Eine Berechtigungsmatrix erlaubt eine präzise und leicht nachvollziehbare Zuordnung von Zugriffsrechten. Das Gruppenprinzip (Einteilung von Benutzern in Gruppen) hilft, Berechtigungen effektiv zu verwalten und vereinfacht die Administration in großen Netzwerken, da man Rechte auf Gruppenebene statt individuell festlegt.
 - **Namens- und Adressierungskonzept:** Eindeutige Namen und Adressen für Netzwerkgeräte erleichtern die Verwaltung und den schnellen Überblick im Netzwerk. Dies ist besonders wichtig für die Netzwerkorganisation und Fehlerbehebung, da es den Administratoren ermöglicht, jede Komponente und deren Standort im Netzwerk sofort zu identifizieren.
 - **Active Directory-Domänendienste:** Ein Active Directory (AD) ermöglicht die zentrale Verwaltung von Benutzern und Zugriffsrechten und stellt sicher, dass Benutzer authentifiziert und ihre Berechtigungen überprüft werden. AD erleichtert die Verwaltung und Kontrolle großer Netzwerke durch klare Strukturen und automatisierte Benutzerverwaltung

## Netzwerkfreigaben in Betriebssystemen mit Linux-Kernel

In Betriebssystemen, die den Linux-Kernel verwenden (wie z.B. Debian, Ubuntu, etc.), können Netzwerkfreigaben mit dem Samba-Programm realisiert werden, das auf dem SMB-Protokoll basiert und CIFS (Common Internet File System) unterstützt. Dateizugriffe über SMB ist eine beliebte Wahl nicht nur, weil Windows als der defacto Standard seit Jahren auf dieses Protokoll setzt, sondern auch, weil macOS und Linux-Derivate dasselbe Protokoll unterstützen. Samba ermöglicht es Linux-basierten Servern, in heterogenen Netzwerken mit Windows- und anderen Systemen zu kommunizieren. Die Konfiguration erfolgt über die Datei smb.conf, und Samba startet über die Prozesse smbd (für Datei- und Druckdienste) und nmbd (für Namensdienste). Auf SMB-Freigaben kann über TCP/IP zugegriffen werden, wodurch sie ähnlich wie Windows-Freigaben genutzt werden können.

Zusätzlich zu Samba verwenden diese Betriebssysteme häufig auch das NFS-Protokoll (Network File System), das speziell für Unix- und Linux-Umgebungen konzipiert ist. NFS ermöglicht die Freigabe von Verzeichnissen über das Netzwerk, sodass diese wie lokale Verzeichnisse eingebunden sind. Die NFS-Konfiguration erfolgt in der Datei /etc/exports und wird durch das Paket nfs-utils verwaltet. Im Gegensatz zu SMB setzt NFS auf Host-basierten Zugriff ohne Benutzerkonto. Berechtigungen werden über die IP-Adressen oder Hostnamen direkt im NFS-Server gesteuert.

## Aufbau von Freigabepfaden

Freigabepfade folgen einer festen Struktur, z.B. `\\HOST\SHARE`, wobei HOST den Servernamen oder die IP-Adresse des Servers und SHARE die freigegebene Ressource bezeichnet. In Windows kann auf Freigaben über den Datei-Explorer oder den Befehl net use zugegriffen werden. In Linux-basierten Systemen erfolgt der Zugriff auf SMB- oder NFS-Freigaben über den mount-Befehl, sodass Benutzer transparent auf Netzwerkressourcen zugreifen können.

## Weiterführende Ressourcen
 - [Youtube - Windows Netzwerkfreigaben](https://www.youtube.com/results?search_query=windows+server+netzwerkfreigaben)
 - [Youtube - Ubuntu Server Samba](https://www.youtube.com/results?search_query=ubuntu+server+samba)
