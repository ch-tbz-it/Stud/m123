# Leistungsbeurteilung - Standard

**Achtung: Für das Modul stehen unterschiedliche Leistungsbeurteilungsmodelle bereit. Informieren Sie sich bei der Lehrperson, welches Modell für Sie gültig ist.**

Die Endnote setzt sich wie folgt zusammen:
| LB               | Beschreibung                               | Anteil      |
|------------------|--------------------------------------------|-------------|
| Kompetenzmatrix  | Erfüllungsgrad erreichte Kompetenzen       | 80 %        |
| Einsatz          | Persönlicher Einsatz und Lernbereitschaft  | 20 %        |

# Schriftliche Prüfungen
An ausgewählten Tagen steht Ihnen die Möglichkeit offen eine oder mehrere schriftliche Prüfungen zu den nachfolgenden Themen abzulegen. Die effektive Durchführung ist mit der Lehrperson zu koordinieren.

| Prüfungstitel                                | Kompetenzen gem. Kompetenzmatrix |
|----------------------------------------------|----------------------------------|
| Grundlagen: Serverhardware und Serverdienste | A1G, A1F, B1G, B1F               |
| DHCP                                         | B2G, B2F                         |
| DNS                                          | B3G, B3F                         |
| Dateifreigabe und Zugriffsschutz             | G1G, B4G                         |

## Prüfungsbedingungen
 - Jede schriftliche Prüfung kann nur einmal gelöst werden.
 - Zeit: maximal 45 Minuten
 - Keine elektronischen Hilfsmittel zugelassen
 - Hilfsmittel: Keine, Ausnahme Zusammenfassung
 - Zusammenfassung: Selbstgeschrieben (keine Gruppenarbeiten, keine kopierte Zusammenfassung vom Kollegen, keine 1:1 kopierte Texte, Grafiken oder Bilder, alle Grafiken müssen selbstgezeichnet sein), maximal 2 A4 Seiten

# Persönlicher Einsatz und Lernbereitschaft
Überzeugen Sie mit Einsatz, Fleiss und Können die Lehrperson. Wenn es für die Lehrperson ersichtlich ist, dass sie sich «ins Zeug gelegt haben», alle grossen und kleinen Hürden mit Hartnäckigkeit und Geduld überwunden haben, dann überzeugen Sie damit die Lehrperson und erhalten entsprechend Punkte. Die Beurteilung erfolgt individuell und berücksichtigt folgendes:

 - Der/Die Lernende(r) hat sich mit den Themen intensiv auseinandergesetzt.
 - Der/Die Lernende(r) beschäftigt sich im Unterricht mit den zu behandelnden Themen.
 - Das Verhalten des/der Lernenden ist der Situation stets angemessen.
 - Der/Die Lernende(r) ist sich der Ziele bewusst und arbeitet auf diese zu.
 - Die vom Lernenden produzierten Produkte sind sauber und sorgfältig umgesetzt.
 - Der/Die Lernende(r) zeigt keine Anzeichen von Gleichgültigkeit. 
 - Der/Die Lernende(r) ist stets zum Unterrichtstart bereit und hört aufmerksam zu. 
 - Der/Die Lernende(r) lässt sich nicht und lenkt andere nicht vom Thema ab. 
 - Der/Die Lernende(r) behandelt seine Mitmenschen anständig und ist stets wohlwollend.

## Benotung

| Beurteilung                                                 | Note       |
|-------------------------------------------------------------|------------|
| sehr gut: Stets hoher Einsatz und hohe Lernbereitschaft     | 5.5 bis 6  |
| gut: Ausreichender Einsatz und hohe Lernbereitschaft        | 4.5 bis 5  |
| ok: Knapp ausreichender Einsatz, mässige Lernbereitschaft   | 4          |
| Kein oder wenig Einsatz, nicht bis wenig Lernbereit         | 3          |











