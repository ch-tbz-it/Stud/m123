# Leistungsbeurteilung

Für das Modul stehen unterschiedliche Leistungsbeurteilungsvarianten bereit. Informieren Sie sich bei der Lehrperson, welches Variante für Sie gültig ist. 

 - [Standard Leistungsbeurteilung](./LB_Standard_2024.md)
 - [Standard Leistungsbeurteilung (ALT)](./LB_Standard_2023.md)
 - [Variante GNS3](./LB_GNS3.md)
