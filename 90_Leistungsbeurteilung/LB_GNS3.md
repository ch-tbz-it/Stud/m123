# Leistungsbeurteilung - Modell GNS3

Für das Modul stehen unterschiedliche Leistungsbeurteilungsmodelle bereit. Informieren Sie sich bei der Lehrperson, welches Modell für Sie gültig ist. 

Die Endnote setzt sich wie folgt zusammen:

| Kriterium                 | Beschreibung                        | Notenpunkte |
|----------------------|-------------------------------------|-------------|
| Labor 1              | DHCP-Konfiguration mit Cisco Router | 0.75        |
| Labor 2              | DHCP-Server auf MikroTik Router     | 0.75        |
| Labor 3              | DNS                                 | 0.75        |
| Labor 4              | TFTP                                | 0.75        |
| Labor 5              | Dateiübertragungsprotokolle         | 0.75        |
| Schriftliche Prüfung | 60 Minuten                          | 1.5         |
| Zusatzaufgaben       | siehe Beschreibung                  | 0.5         |
| Qualität und Einsatz | siehe Beschreibung                  | 0.75        |
| **Total**            |                                     | **6.5**      |

## Laborübungen
Jeder der fünf Laborübungen werden wie folgt bewertet:

| Kr. | Beschreibung                                                                   | Notenpunkte |
|-----|--------------------------------------------------------------------------------|-------------|
| 1   | Laboraufgabe gemäss Vorgabe im persönlichen Repository vollständig dokumentiert.            | 0.25        |
| 2   | Die Lösung der Laboraufgabe enthält keine oder nur wenige Fehler.              | 0.25        |
| 3   | Der/Die Lernende kann auf Nachfrage der Lehrperson die Laboraufgabe erklären.  | 0.25        |
|     | **Total**                                                                          | **0.75**        |

Jede Abgabe einer Laborübung wird grundsätzlich nur einmal bewertet. 


## Qualität und Einsatz
Der/die Lernende überzeugt mit Einsatz, Fleiss und Können die Lehrperson. In den Unterlagen des Lernenden ist ersichtlich, dass er/sie sich ins "Zeug gelegt hat", grosse und kleine Hürde mit Hartnäckigkeit und Geduld überwunden hat. Im Endprodukt sind die zu erreichenden Kompetenzen ersichtlich. Die persönliche Lernentwicklung ist beim Lernenden eindeutig erkennbar (Vergleich zum Vorwissen, Wissenszuwachs). Der/die Lernende hat sich bei Schwierigkeiten frühzeitig Hilfe gesucht. 
 - 0.75 Notenpunkte = sehr gut: Brillante Arbeiten, übertrifft Erwartungen
 - 0.5 Notenpunkte = gut: Tolle Arbeit, Einsatz ausreichend
 - 0.25 Notenpunkte = ok: Beim Einsatz oder bei der Arbeit liegt noch mehr Potential drin
 - 0 Notenpunkte = Naja, hat etwas gemacht. Einsatz fast nicht erkennbar oder unzureichend.
 
Die Beurteilung erfolgt individuell und berücksichtigt folgendes:
-	Originalität, Kreativität, Komplexität
-	Persönliche Lernentwicklung (Vergleich zum Vorwissen, Wissenszuwachs)
-	Sauberkeit der Umsetzung
-	Hartnäckigkeit, Einsatz und Interesse der Lernenden
-	Pünktlichkeit
-	Lernende haben Lehrperson bei Schwierigkeiten frühzeitig aufgesucht
-	Dokumentation (Schriftlich PDF, Repository, Video)
-	Kontinuierliche Weiterentwicklung, Arbeit gut aufgeteilt (Nicht alles in den letzten 5 Tagen vor Abgabe). 
-	Lernende können ihren Arbeitseinsatz nachweisen (Saubere Dokumentation, Regelmässige Präsentation, Video, usw.)
-	Einsatz von Tools zur Codeverwaltung und Arbeitsorganisation (z.B. Gitlab Repository mit Issues, usw.)

## Schriftliche Leistungsbeurteilung

|   |   |
|---|---|
| Dauer  | 1 Lektion  |
| Erlaubte Hilfsmittel | Selbstgeschriebene Zusammenfassung (siehe Hinweise) |
| Zulassungsbedingung | keine |
| Nicht erfüllen der Zulassungsbedingungen | N/A |
| Elektronische Hilfsmittel | Nicht erlaubt |

Der Prüfungsinhalt umfasst alle bis dahin besprochenen
 Unterlagen. Der definitive Prüfungsinhalt wird zwei Wochen vorher bekanntgegeben. 

<br>Unterlagen: *Präsentationen*, *GitLab Repository zum Modul*, *Unterlagen auf Teams*

## Persönliche Zusammenfassung für schriftliche Lernkontrollen
 - Umfang:
   - Ausgedruckt: Max. 2 x A4 Papier (=4 Seiten)
   - Handgeschrieben: Max 4 x A4 Papier (=8 Seiten)
 - Individuelle und selbstgeschriebene Zusammenfassung
 - Keine „Collagen“: Aus den Unterlagen 1:1 übernommenen Text oder
Seitenausschnitte sind nicht zugelassen.
   - Ausschnitte von einzelnen Grafiken / Schemas ist erlaubt.
 - Mindestumfang: Wichtigste Punkte jedes prüfungsrelevanten Themas
 - Zusammenfassung muss mit der Prüfung abgegeben werden!

# Zusatzaufgaben

Die Bewertung der Zusatzaufgaben erfolgt individuell:
 - Gemäss Beschreibung in der Zusatzaufgabe
 - In mündlicher Absprache mit der Lehrperson

Maximal können mit Zusatzaufgaben Notenpunkte gemäss obiger Tabelle erreicht werden. 