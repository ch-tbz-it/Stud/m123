# Leistungsbeurteilung - Standard

Für das Modul stehen unterschiedliche Leistungsbeurteilungsmodelle bereit. Informieren Sie sich bei der Lehrperson, welches Modell für Sie gültig ist. 

Die Endnote setzt sich wie folgt zusammen:
| LB               | Beschreibung                               | Anteil      |
|------------------|--------------------------------------------|-------------|
| LB1              | Schriftliche Leistungsbeurteilung 1        | 20 %        |
| LB2              | Schriftliche Leistungsbeurteilung 2        | 20 %        |
| Übungen          | Teilnahme an Übungen im Unterricht         | 40 %        |
| Einsatz          | Persönlicher Einsatz und Lernbereitschaft  | 20 %        |

# Schriftliche Leistungsbeurteilung 1 & 2
 - Die schriftliche Leistungsbeurteilung dauert 45 - 60 Minuten
 - Der genaue Prüfungsstoff wird 1 Woche im voraus bekanntgegeben. 

# Teilnahme an Übungen im Unterricht

Pro bewertete Übung:

| Nr. | Kriterium                                                            | Punkte  |
|-----|----------------------------------------------------------------------|---------|
| 1   | Leistungsbereitschaft und Qualität: <br\>Unzureichend (= 0P), Ausreichend (=1P), Überdurchschnittlich (=2P) | 2 P |
| 3   | Pünktlich abgegeben                                                  | 1 P     |
| 4   | Übung gelöst                                                         | 1 P     |

 - Die totale Punktzahl hängt von der Anzahl Pflichtübungen ab. 

## Notenbereiche
| Kompetenzband   | Notenbereich |
| --------------- | ------------ |
| Grundlagen      | 4 bis 5      |
| Fortgeschritten | 5 bis 6      |
| Erweitert       | 5.5 bis 6    |

# Persönlicher Einsatz und Lernbereitschaft
Überzeugen Sie mit Einsatz, Fleiss und Können die Lehrperson. Wenn es für die Lehrperson ersichtlich ist, dass sie sich «ins Zeug gelegt haben», alle grossen und kleinen Hürden mit Hartnäckigkeit und Geduld überwunden haben, dann überzeugen Sie damit die Lehrperson und erhalten entsprechend Punkte. Die Beurteilung erfolgt individuell und berücksichtigt folgendes:

 - Der/Die Lernende(r) hat sich mit den Themen intensiv auseinandergesetzt.
 - Der/Die Lernende(r) beschäftigt sich im Unterricht mit den zu behandelnden Themen.
 - Das Verhalten des/der Lernenden ist der Situation stets angemessen.
 - Der/Die Lernende(r) ist sich der Ziele bewusst und arbeitet auf diese zu.
 - Die vom Lernenden produzierten Produkte sind sauber und sorgfältig umgesetzt.
 - Der/Die Lernende(r) zeigt keine Anzeichen von Gleichgültigkeit. 
 - Der/Die Lernende(r) ist stets zum Unterrichtstart bereit und hört aufmerksam zu. 
 - Der/Die Lernende(r) lässt sich nicht und lenkt andere nicht vom Thema ab. 
 - Der/Die Lernende(r) behandelt seine Mitmenschen anständig und ist stets wohlwollend.

## Benotung

| Beurteilung                                                 | Note       |
|-------------------------------------------------------------|------------|
| sehr gut: Stets hoher Einsatz und hohe Lernbereitschaft     | 5.5 bis 6  |
| gut: Ausreichender Einsatz und hohe Lernbereitschaft        | 4.5 bis 5  |
| ok: Knapp ausreichender Einsatz, mässige Lernbereitschaft   | 4          |
| Kein oder wenig Einsatz, nicht bis wenig Lernbereit         | 3          |











