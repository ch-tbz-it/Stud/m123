# Zugriffsschutz

Zugriffsschutz ist zentral, um sensible Daten und Ressourcen vor unbefugtem Zugriff zu schützen und sicherzustellen, dass nur autorisierte Nutzer Zugang haben. Auf Netzwerkebene schützt Zugriffsschutz vor externen Bedrohungen durch Firewalls und Zugriffskontrollen, während er auf Dateisystemebene sicherstellt, dass sensible Dateien nur von befugten Nutzern eingesehen und bearbeitet werden können. Physischer Zugriffsschutz umfasst Massnahmen wie Zugangsbeschränkungen zu Serverräumen, um die Hardware vor Manipulation zu schützen. Das Verständnis der verschiedenen Arten von Zugriffsschutz (G1G) ist entscheidend, um angemessene Schutzmassnahmen sowohl im Netzwerk als auch im Dateisystem umzusetzen (G1F). Ein kombinierter Zugriffsschutz, der Benutzer- und Gruppenrechte wie das AGDLP-Prinzip einbezieht (G1E), ermöglicht es, Zugriffsrechte flexibel und effektiv zu verwalten. Zugriffsschutz ist nicht nur für den Schutz geistigen Eigentums und finanzieller Daten unerlässlich, sondern auch gesetzlich gefordert, etwa durch das Datenschutzgesetz, das den Schutz personenbezogener Daten sicherstellt.

## Übung - Übersicht über Zugriffsschutzmassnahmen

### Kompetenz
G1G: Ich kann verschiedene Arten von Zugriffsschutz im Netzwerk und auf dem Dateisystem erklären. 

### Ziel
Ziel dieser Übung ist es Zugriffsschutzmassnahmen für einen Anwendungszweck zu definieren. 

### Aufgabenstellung
1. Skizziere eine Netzwerktopologie für ein kleines KMU mit drei Mitarbeitern und einem Dateiserver. Überlege dir was für Komponenten  benötigt werden und wie die Kompetenzen untereinander verbunden sind. 
2. Überlege dir was es für Gefahren gibt. 
3. Definiere für jede Komponente in der Netzwerktopologie wie diese vor unberechtigten Zugriff geschützt wird. Unterscheide bei den Massnahmen jeweils zwischen physischen und softwaretechnischen Zugriffsschutzmassnahmen. 
4. Bespreche dein Resultat mit der Lehrperson. 

