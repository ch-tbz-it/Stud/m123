# Repetition IP

Im [Modul 117](https://www.modulbaukasten.ch/module/117) wurden die Grundkompetenzen in den Themen Switching und IPv4 erworben. Diese Kompetenzen werden zur Konfiguration von Serverdiensten benötigt. Dieses Kapitel hat zum Ziel diese Themen zu repetieren. 

# Kompetenzen
 - **Modul 117:** 2.2 Kennt die wichtigsten Regeln für eine korrekte Netzwerkkonfiguration (IP-Adressformat, Subnetzmaske, private Adressen, Standardgateway, DNS) und kann diese anhand von Beispielen erläutern.
 - **Modul 117:** 2.3 Kennt die prinzipiellen Aufgaben der Netzwerkkomponenten Switch, Accesspoint und Router und kann aufzeigen, wo und zu welchem Zweck diese in einem Netzwerk eingesetzt werden.

# Übungen
 - [1. BeASwitch](1_Übung_BeASwitch.md) - Wer Switching versteht kann auch ein Switch sein. 
 - *Optional:* [2. BeARouter](2_Übung_BeARouter.md) - Wer ~~Switching~~Routing versteht kann auch ein ~~Switch~~Router sein.  
 - *Optional:* Zusatzübung: [3. IPv4 Quiz](3_Übung_IPv4_Quiz.md) - Mit Repetition IPv4 Berechnungen beherrschen.

# Weiterführende Links
 - [Repetition im Modul 129 - IPv4](https://gitlab.com/ch-tbz-it/Stud/m129/-/tree/main/50_IPv4)