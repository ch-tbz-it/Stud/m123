# Fehlersuche im Netzwerk

Ziel dieser Übung ist es, die Grundlagen aus Modul 117 zu wiederholen und erste Erfahrungen mit dem Cisco Packet Tracer zu sammeln. 

 - **Sozialform:** Einzelarbeit
 - **Aufgabenstellung und Lernziele:** Siehe Word-Dokument
 - **Lernprodukt:** Ausgefülltes Word-Dokument, Vollständig funktionierendes Cisco Packet Tracer Labor
 - **Überprüfung:** Die Aufgabe ist mit der Lehrperson mündlich zu besprechen. 

**Hinweis zu der Labordatei:** Nutze die nachfolgende Tabelle und aufgrund des Anfangsbuchstabens deines Nachnamen die Labordatei auszuwählen. (Wenn dein Anfangsbuchstabe nicht vorkommt, nimm Labor 13.)

|   Datei | Anfangsbuchstabe Nachname   |
|--------:|:----------------------------|
|       1 | F, A                        |
|       2 | S, Z                        |
|       3 | K, E                        |
|       4 | P, C                        |
|       5 | H, N                        |
|       6 | B, J                        |
|       7 | M, V                        |
|       8 | G, O                        |
|       9 | R, I                        |
|      10 | W, U                        |
|      11 | L, Y                        |
|      12 | T, Q                        |
|      13 | D, X                        |

 - Die Labordateien findest du hier: [Labordateien](./Labore/)
 - Das [Word-Dokument mit der Aufgabenstellung](./01_Fehlersuche%20im%20Netzwerk%20mit%20Cisco%20Packet%20Tracer%20V3.docx)
 