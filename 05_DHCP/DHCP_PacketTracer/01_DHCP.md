# Laborübung 1 - DHCP mit Cisco Packet Tracer

Ziel dieser Laborübung ist eine DHCP Konfiguration zu analysieren, DHCP Lease (*IP address assignment*) zu beobachten und anschliessend Konfigurationsänderungen vorzunehmen.

# Lernziele
 - Kennt den prinzipiellen Unterschied zwischen einer dynamischen und einer statischen Zuweisung von IP Adressen und kann aufzeigen, in welchem Anwendungsfall eine dynamische resp. eine statische Adress-Zuweisung sinnvoll ist.
 - Kennt die Einstellungen zur Konfiguration eines DHCP Servers und kann erläutern, wie diese Einstellgrössen die Zuweisung einer IP Adresse, einer Subnet-Maske und allenfalls Angaben zu DNS-Servern und Standard-Gateways bei der Anfrage eines Clients beeinflussen.

# Voraussetzung
 - *Cisco Packet Tracer*-Vorlage geöffnet (Die Vorlagen sind hier zu finden: [Vorlagen/labs](Vorlagen/labs/). Die Gruppeneinteilung erfolgt überlicherweise durch den Kursleiter/Leherperson.)

# Aufgabe
Die nachfolgenden Abschnitte sind Schritt-für-Schritt durchzugehen. Halten Sie alle Antworten und getätigten Konfigurationen in ihrem persönlichen Lernjournal fest. 

# Ausgangslage
![Ausgangslage](media/Ausgangslage.PNG)

Im nachfolgenden Netzwerk sind allen Geräte ausser PC3 per DHCP eine IPv4 Adresse zugewiesen worden. Der Router *R1* agiert als DHCP Server und verteilt IP Adressen. 

*Tipp: Starten Sie alle Geräte nach dem Öffnen des Labors neu. Drücken Sie dazu unten Links im *Packet Tracer* auf das nachfolgende Symbol:
![reset button cisco packet tracer](media/reset.PNG)

Anschliessend empfiehlt es sich 1 Minute zu warten, bevor mit den Aufgaben fortgefahren wird, damit alle Geräte Zeit haben per DHCP eine IPv4-Adresse zu beziehen. 

# 1. Router-Konfiguration auslesen
Im ersten Schritt gilt es herauszufinden in welchem Subnet und welchem Range der DHCP Server IPv4-Adressen im lokalen Netzwerk verteilt. Dafür gibt verschiedene Befehle, die in Ihrem Output die entsprechenden Optionen beinhalten. Die Befehle können in der *CLI* des Routers ausgeführt werden. 

Damit die Befehle ausgeführt werden können, muss sich der Router im richtigen Modus befinden. Öffnen Sie die *CLI* und drücken Sie solange die Enter Taste bis `R1>` oder ähnlich erscheint. Der Router ist nun bereit für ihre Befehle.

Das `>` Zeichen in `R1>` zeigt dem Administrator an, dass er sich im `User Mode` befindet. Ist dies der Fall, wechseln Sie in den `Enabled Mode` indem sie den Befehl `enable` eingeben und anschliessend *Enter* drücken. Ob erfolgreich in den `enabled`-Mode gewechselt wurde, sehen Sie daran, dass nun das `#`-Symobol in der Befehlzeile steht. 

Probieren Sie die nachfolgenden Befehle aus und versuchen Sie die nachfolgenden Fragen zu beantworten:
 - Für welches Subnetz ist der DHCP Server aktiv?
 - Welche IP-Adressen ist vergeben und an welche MAC-Adressen? (Antwort als Tabelle)
 - In welchem Range vergibt der DHCP-Server IPv4 Adressen?
 - Was hat die Konfiguration `ip dhcp excluded-address` zur Folge?
 - Wie viele IPv4-Adressen kann der DHCP-Server dynamisch vergeben?

Befehle:
 - `show ip dhcp pool`
 - `show ip dhcp binding`
 - `show running-config`

# 2. DORA - *DHCP Lease* beobachten
Mit dem *Cisco Packet Tracer* können Ethernet Frames oder allgemeiner *Protocol Data Unit* (PDU) sehr gut nachverfolgt werden. In diesem Schritt soll eine DHCP IPv4-Adress-Zuteilvorgang mitverfolgt werden. 

 1. *PC2* dazu bringen eine neuen DHCP-Request zu starten. Dafür im Menü des PCs unter *Config => FastEthernet0* die IP Konfiguration kurz von *DHCP* auf *Static* setzen. 
 2. Unten rechts im *Packet Tracer* auf *Simulation* wechseln.
 3. Im Menü des *PC2* unter *Config => FastEthernet0* die IP Konfiguration kurz von *Static* wieder auf *DHCP* setzen. 
![poweroff pc](media/poweroff.PNG)
 3. *PC2* wieder starten.
 4. Es erscheint sofort ein gelbes PDU beim PC2 (bzw. beim PC bei dem diese Schritte gerade ausgeführt werden). 
![PC with PDU](media/pcwithdhcpdiscover.PNG)
 5. Mit einem klick kann das *PDU* geöffnet und analysiert werden. 
![Open DHCP Discover PDU](media/packettracer_ms_doku.PNG)

*Hinweis:* Teilweise verschwindet der DHCP-Discover sobald das *Properties-Window* des Gerätes geschlossen wird: Lassen Sie das *Properties-Window* einfach offen.

Unter *Outbound PDU Details* finden sehen wir den hierarchischen Aufbau des PDUs mit den verschiedenen Layern. Unterhalb von *IP* ist der *DHCP* Layer zu finden. Mithilfe der [Microsoft DHCP Informationsseite](https://learn.microsoft.com/en-us/windows-server/troubleshoot/dynamic-host-configuration-protocol-basics) können wir herausfinden, dass es sich hierbei um einen DHCP Discover handelt, da der *OP Code* den Wert 1 hat. 

Ziel ist es nun alle vier PDUs zu finden und zu identifzieren. 

Fragen:
 - Welcher OP-Code hat der DHCP-Offer? 
 - Welcher OP-Code hat der DHCP-Request?
 - Welcher OP-Code hat der DHCP-Acknowledge?
 - An welche IP-Adresse wird der DHCP-Discover geschickt? Was ist an dieser IP-Adresse speziell?
 - An welche MAC-Adresse wird der DHCP-Discover geschickt? Was ist an dieser MAC-Adresse speziell?
 - Weshalb wird der DHCP-Discover vom Switch auch an PC3 geschickt?
 - Gibt es ein DHCP-Relay in diesem Netzwerk? Wenn ja, welches Gerät ist es? Wenn nein, wie kann das mithilfe der DHCP-PDUs festgestellt werden?
 - Welche IPv4-Adresse wird dem Client zugewiesen?


Auf den Screenshots sollten *alle* Felder von DHCP sichtbar sein:
 - Screenshot des DHCP-Discovers PDUs
 - Screenshot des DHCP-Offers PDUs
 - Screenshot des DHCP-Request PDUs
 - Screenshot des DHCP-Acknowledge PDUs

# 3. Netzwerk umkonfigurieren
Aktuell erhält der Server seine IPv4-Adresse vom DHCP-Server. Diese IPv4-Adresse ist nicht statisch zugewiesen und kann sich zum Beispiel beim Neustart des Routers oder des Servers ändern. Grundsätzlich kann der Router auch nach Ablauf der *DHCP lease time* eine andere IPv4-Adresse zuteilen. 

Um das zu verhindern weisen wir dem Server eine statische IPv4-Adresse zu. 

## Aufgabe
Dem Server eine statische IPv4-Adresse zu weisen. Nehmen Sie eine IPv4-Adresse, die sich leicht merken lässt. 

## Hinweise
 - Die statisch zugewiesene IPv4-Adresse muss ausserhalb des DHCP-Bereichs liegen. Konsultieren Sie hierzu Ihre Antworten aus Aufgabe 1. 
 - Die IPv4-Adresse im Menü des Servers unter *Config* -> *FastEthernet0/0* konfiguriert werden. 
 - Das Gateway kann in Menü unter *Config* -> *Settings* festgelegt werden.

Im Laborbericht sind folgende Screenshots zu hinterlegen:
 - Screenshot Konfigurationsmenü Server mit Sichtbarer IPv4-Adresse vom Interface FastEthernet
 - Screenshot Konfigurationsmenü Server mit Sichtbarer IPv4-Adresse des Gateways
 - Screenshot der Webseite des Servers auf einem der PCs inkl. sichtbarer IPv4-Adresse des Servers

Prüfen Sie anschliessend, ob der Server von PC1 erreichbar ist. 
 - `ping 192.168.X.X` (IP-Adresse des Servers einsetzen) unter *Desktop* -> *Command Prompt*
 - Auf dem Server ist eine Webseite aktiv. Mit dem Webbrowser zugreifen *Desktop* -> *Web Browser*

