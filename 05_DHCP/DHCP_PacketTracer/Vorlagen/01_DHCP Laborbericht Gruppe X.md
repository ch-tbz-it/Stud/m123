# Laborübung 1 - DHCP mit Cisco Packet Tracer

Name: 
Datum: 

# 1. Router-Konfiguration auslesen
 - Für welches Subnetz ist der DHCP Server aktiv?
 - Welche IP-Adressen ist vergeben und an welche MAC-Adressen? (Antwort als Tabelle)
 - In welchem Range vergibt der DHCP-Server IPv4 Adressen?
 - Was hat die Konfiguration `ip dhcp excluded-address` zur Folge?
 - Wie viele IPv4-Adressen kann der DHCP-Server dynamisch vergeben?

# 2. DORA - *DHCP Lease* beobachten
 - Welcher OP-Code hat der DHCP-Offer? 
 - Welcher OP-Code hat der DHCP-Request?
 - Welcher OP-Code hat der DHCP-Acknowledge?
 - An welche IP-Adresse wird der DHCP-Discover geschickt? Was ist an dieser IP-Adresse speziell?
 - An welche MAC-Adresse wird der DHCP-Discover geschickt? Was ist an dieser MAC-Adresse speziell?
 - Weshalb wird der DHCP-Discover vom Switch auch an PC3 geschickt?
 - Gibt es ein DHCP-Relay in diesem Netzwerk? Wenn ja, welches Gerät ist es? Wenn nein, wie kann das mithilfe der DHCP-PDUs festgestellt werden?
 - Welche IPv4-Adresse wird dem Client zugewiesen?

Auf den Screenshots sollten *alle* Felder von DHCP sichtbar sein:
 - Screenshot des DHCO-Discovers PDUs
 - Screenshot des DHCO-Offers
 - Screenshot des DHCO-Request
 - Screenshot des DHCO-Acknowledge

# 3. Netzwerk umkonfigurieren
 - Screenshot Konfigurationsmenü Server mit Sichtbarer IPv4-Adresse vom Interface FastEthernet
 - Screenshot Konfigurationsmenü Server mit Sichtbarer IPv4-Adresse des Gateways
 - Screenshot der Webseite des Servers auf einem der PCs inkl. sichtbarer IPv4-Adresse des Servers