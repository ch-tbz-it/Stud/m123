# Laborübung 2: DHCP Server auf MikroTik Router

![Symbolbild Laborübung 2](media/Labor_MikroTik_Ausgangslage.PNG)

Im vorhergehenden Labor wurden folgende Ziele erreicht:
 - Konsole geöffnet und erste Befehle ausgeführt
 - DHCP Konfiguration aus Cisco Router ausgelesen und dokumentiert
 - *DHCP lease* mit Wireshark beobachtet
 
 *Ziel dieser Laborübung:* DHCP-Server nach Vorgabe auf MikroTik Router einrichten. 
 
 Als Vorgabe dienen die DHCP Parameter aus der vorhergehenden Übung. Schlussendlich sind die beiden Router (rein funktional) identisch konfiguriert (bis auf Herstellerspezifische Eigenheiten).

# Lernziele
 - Kennt den prinzipiellen Unterschied zwischen einer dynamischen und einer statischen Zuweisung von IP Adressen und kann aufzeigen, in welchem Anwendungsfall eine dynamische resp. eine statische Adress-Zuweisung sinnvoll ist.
 - Kennt die Einstellungen zur Konfiguration eines DHCP Servers und kann erläutern, wie diese Einstellgrössen die Zuweisung einer IP Adresse, einer Subnet-Maske und allenfalls Angaben zu DNS-Servern und Standard-Gateways bei der Anfrage eines Clients beeinflussen.

# Grundsätzliches Vorgehen
In dieser Aufgabe gibt es keine Schritt für Schritt Anleitung. Das bedeutet, dass das Vorgehen, um das oben beschrieben Ziel (DHCP-Server nach Vorgabe auf MikroTik Router einrichten.) zu erreichen, selbst geplant werden muss. Eine mögliche Herangehensweise:
 - Alle Informationen zur Aufgabe lesen (d.h. diesen gesamten Artikel). 
 - Alle Links in diesem Dokument öffnen: Helfen sie dir weiter?
 - Zugang zu allen benötigten Ressourcen prüfen (z.B. Kann ich einen MikroTik Router in GNS3 hinzufügen, kann ich den Router starten und mich einloggen?)
 - Danach, bis erfolgreich gelöst:
   - These aufstellen: "Mit diesen Befehlen kann ich den DHCP-Server konfigurieren."
   - These umsetzen: Entsprechende Befehle eingeben
   - These prüfen: "Bekommen meine Hosts eine IP-Adresse?"

# Weitere Hinweise zur Aufgabe
In dieser Laborübung gibt es keine Schritt-für-Schritt Anleitung. Folgendes steht zur Verfügung:
 - Login MikroTik: *admin* Passwort: *keines gesetzt* ==> Immer Passwort *admin* setzen!
 - Links zu weiterführenden Quellen
 - Liste von nützlichen Befehlen

# Vorbereitung
![Vorbereitungen](media/VorbereitungenLabor2.PNG)

Bauen Sie das Netzwerk Analog wie in Laborübung 1 auf. Als Router ist ein *MikroTik CHR* zu verwenden.

Nachdem der MikroTik Router gestartet wurde, dass Passwort festgelegt wurde, erfolgt die Grundkonfiguration mit den nachfolgenden Befehlen:
1. Name bzw. Identität des Routers:
```
/system identity set name=R1
```
2. IP Adresse auf LAN-*interface* programmieren:
```
/ip address add address=192.168.10.1/24 interface=ether2 network=192.168.10.0
```
3. DNS Anfragen erlauben von anderen Hosts erlauben
```
/ip dns set allow-remote-requests=yes
```
4. NAT *Masquerade* aktivieren.<br> Dies wird benötigt, da die Adressen auf der LAN Seite des Routers im WAN nicht bekannt sind. Mehr zu NAT im Modul 129. 
```
/ip firewall nat add action=masquerade chain=srcnat out-interface=ether1
```

# Wichtig
 - Beim ersten Login in den MikroTik Router muss ein Passwort festgelegt werden. Es ist das Passwort `admin` festzulegen. (Standardlogin ist Benutzername `admin` ohne Paswort). 
 - In der VM-Konfiguration des MikroTik Routers (Kontextmenü => Configure) kann unter *Network* die Anzahl der Netzwerkadapter festgelegt werden. Es wird empfohlen diesen Wert auf 4 zu stellen. 

# Befehle
Die nachfolgenden Befehle sind lediglich zur Unterstützung / Anhaltspunkt aufgeführt: 
 - `/ip/dhcp-client print` - Per DCHP erhaltene IP-Adresse anzeigen.
 - `/ip/address print` Konfigurierte IP-Adressen anzeigen
 - `/ip/dhcp-server print` DHCP Server instanzen anzeigen
 - `/ip dhcp-server/network/print` DHCP Netzwerke anzeigen
 - `/ip pool/print` Konfigurierte IP Pools anzeigen
 - `/export` Gesamte Konfiguration anzeigen

Anstatt `print` am Ende des Befehls kann mit `export` die Konfiguration angezeigt werden. Beispiel: `/ip dhcp-client/export`

# Empfohlenes Vorgehen
Für eine reibungslose Konfiguration wird empfohlen wie folgt vorzugehen:
 - Hostname konfigurieren
 - Konfigurierte IPv4-Adressen überprüfen
 - Konfigureirte DHCP-Clients überprüfen (Erlernte IPv4-Adressen müssten im vorhergehenden Schritt angezeigt werden.)
 - IPv4-Adressen konfigurieren
 - DHCP-Setup durchführen

# Beispiel: IPv4-Adressen konfigurieren
Die IP-Adressen auf der LAN und WAN-Seite wurden bereits bei der Vorbereitung konfiguriert. Wie diese Konfiguration aus der Cisco Konfiguration entnommen und in MikroTik Befehle umgesetzt werden kann, erfahren Sie in diesem Abschnitt. 

Aus der Konfiguration des Cisco Routers `R1` (vorherige Laborübung) können aus der Konfiguration folgende Zeilen entnommen werden:
```
interface GigabitEthernet0/0
 ip address dhcp
 ip nat outside
 ip virtual-reassembly in
 duplex auto
 speed auto
 media-type rj45
!
interface GigabitEthernet0/1
 ip address 192.168.10.1 255.255.255.0
 ip nat inside
 ip virtual-reassembly in
 duplex auto
 speed auto
 media-type rj45
```

Daraus lässt sich ableiten:
 - Der Router erlernt die IPv4-Adresse aus dem *interface* `GigabitEthernet0/0` mithilfe von DHCP. 
 - Auf dem *interface* `GigabitEthernet0/1` ist die IP Adresse `192.168.10.1/24` konfiguriert.

Zuerst prüfen wir die konfigurierten IPv4-Adressen mit dem Befehl `/ip address/print`:
```
[admin@R1] > /ip address/print
Flags: D - DYNAMIC
Columns: ADDRESS, NETWORK, INTERFACE
#   ADDRESS             NETWORK        INTERFACE
0 D 192.168.122.252/24  192.168.122.0  ether1
```
Dem *interface* `ether1` wurde die IP-Adresse `192.168.122.252/24` dynamisch (angedeutet mit dem Flag `D`) zugewiesen. 

Im nächsten Schritt gilt es herausfinden, mit welchem Service die IPv4-Adresse zugewiesen wurde. Das gängiste Protokoll ist DHCP. Der Befehl `/ip dhcp-client/print` gibt die konfigurierten DHCP-Clients und dessen Status zurück. 
```
[admin@R1] > /ip dhcp-client/print
Columns: INTERFACE, USE-PEER-DNS, ADD-DEFAULT-ROUTE, STATUS, ADDRESS
# INTERFACE  USE-PEER-DNS  ADD-DEFAULT-ROUTE  STATUS  ADDRESS
0 ether1     yes           yes                bound   192.168.122.252/2
```
Die IPv4-Adresse und das *interface* stimmen mit der Ausgabe des vorherigen Befehles `/ip address/print` überein. 

Daraus lässt sich schliessen, dass in der Standardkonfiguration des MikroTik Routers ein DHCP-Client auf der ersten Netzwerkschnittstelle - sprich *interface* `ether1`- aktiv ist. 

Übrig bleibt nur noch die Konfiguration der IPv4-Adresse für das zweite *interface*. 

Der Befehl `/interface/print` gibt alle verfügbaren physischen und virtuellen Netzwerkschnittstellen aus:
```
[admin@R1] > /interface/print
Flags: R - RUNNING
Columns: NAME, TYPE, ACTUAL-MTU, MAC-ADDRESS
#   NAME    TYPE   ACTUAL-MTU  MAC-ADDRESS
0 R ether1  ether        1500  0C:B3:54:12:00:00
1   ether2  ether        1500  0C:B3:54:12:00:01
```
Aus der Netzwerkgrafik im GNS3 und weil ether2 die einzig Verfügbare Netzwerkschnittstelle ist, wissen wir, dass die IPv4-Adresse für das interne Netzwerk auf `ether2` konfiguriert werden muss. 

Der Befehl kann aus der MikroTik konfiguration entnommen werden:
```
[admin@R1] > /ip address/add address=[IPv4 adress with subnet] interface=[name of the interface]
```
Durch welche Werte müssen `[IPv4 adress with subnet]` und `[name of the interface]` ersetzt werden? (Tipp: Die eckigen Klammern müssen weg.)

Treffen Sie eine Annahme. 

Ausprobieren, Fehler machen, Aus Fehler lernen => Erfolgserlebnis!

# Hinweise
 - Alle Befehle im Lernjournal dokumentieren!

# Weiterführende Ressourcen
 - MikroTik CLI: https://help.mikrotik.com/docs/display/ROS/Command+Line+Interface
 - MikroTik DHCP Server Konfigurationsbeispiel: https://help.mikrotik.com/docs/display/ROS/DHCP#DHCP-ConfigurationExamples.2
 - MikroTik Befehle Cheat-Sheet: http://www.mkesolutions.net/pdf/routeros-cheat-sheet-v1.1.pdf
 - *Video* MikroTik CLI Einführung: https://www.youtube.com/watch?v=EYCjuvTd3dY
