# Laborübung 1: DHCP Server-Konfiguration aus einem Cisco Router auslesen

Ziel dieser LaboüÜbung ist eine DHCP Konfiguration zu analysieren, DHCP *ip address assignment* zu beobachten und anschliessend DHCP zu konfigurieren. 

# Lernziele
 - Kennt den prinzipiellen Unterschied zwischen einer dynamischen und einer statischen Zuweisung von IP Adressen und kann aufzeigen, in welchem Anwendungsfall eine dynamische resp. eine statische Adress-Zuweisung sinnvoll ist.
 - Kennt die Einstellungen zur Konfiguration eines DHCP Servers und kann erläutern, wie diese Einstellgrössen die Zuweisung einer IP Adresse, einer Subnet-Maske und allenfalls Angaben zu DNS-Servern und Standard-Gateways bei der Anfrage eines Clients beeinflussen.

# Voraussetzung
 - OpenVPN Verbindung zum GNS3 Labor Server aufgebaut
 - GNS3 mit dem GNS3 Labor Server verbunden

# Ausgangslage
![GNS3 Projekt DHCP Labor](media/GNS3_Ausgangslage.PNG)

Die Ausgangslage besteht aus einem GNS3 Projekt. Dieses beinhaltet ein fertig konfiguriertes Netzwerk bestehend aus einem Router, einem *unmanaged* Switch, sowie drei Enduser-PCs. 

# Aufgabe
Die nachfolgenden Abschnitte sind Schritt-für-Schritt durchzugehen. Halten Sie alle Antworten und getätigten Konfigurationen in ihrem persönlichen Repository fest. 

## 1. Projekt importieren
1. Beim Start von GNS wird das *Project* Fenster angezeigt. Klicken Sie auf unten rechts auf *Cancel*
2. *File* -> *Import Portable Project*, anschliessend *01_DHCP.gns3project* suchen und öffnen.
3. Im neuen Fenster *Name* anpassen: Den Namen das persönliche Kürzel/Vorname/o.ä. anfügen. Beispiel: *01_DHCP_ALP*. Anschliessend auf OK drücken. 

## 2. Router starten, einloggen und konfigurierte IP-Adressen auslesen
Im ersten Teil der Übung haben wir das Ziel herauszufinden, welche IP-Adressen auf welchen *Interfaces* des Routers konfiguriert sind. Auf dem Netzwerkschema erkennen wir, dass vom Router zwei Verbindungen weggehen: eine mit der Beschriftung *Gi0/0* in Richtung *NAT1* und eine mit der Beschriftung *Gi0/1* in Richtung *Switch*.

1. Router starten: Wählen Sie im Kontextmenü des Routers *R1* *Start* aus.  
2. Konsole öffnen: Wählen Sie im Kontextmenü des Routers *R1* *Console* aus.  
3. In der Konsole können Sie nun den Bootvorgang des Routers mitverfolgen. 
4. Warten Sie bis zu 2 Minuten und drücken Sie ein paar mal die Enter Taste. Vergleiche Abbildung 1.

![Router aufgestartet Konsole bereit](media/Router_aufgestartet.PNG)

5. *R1* steht für den konfigurierten Namen des Routers. Das *#* Symbol steht für den aktuellen Modus: EXEC Enable Mode. 
6. **CLI Hilfe Funktionen:** Geben Sie `show ?` ein. Der Router gibt Ihnen eine Liste von möglichen Argumenten zurück. 
7. **Pagination:** Wenn `--More--` angezeigt wird, drücken Sie die Leertaste um noch mehr mögliche Argumente anzuzeigen oder die Taste *Q* um vorzeitig abzubrechen. 
8. Sie sollte nun `R1#show ` in der letzten Zeile sehen. Verfollständigen Sie den Befehl indem Sie `ip interface brief` anfügen. Die gesamte Zeile sollte so aussehen:
```cisco
R1#show ip interface brief
```
Drücken Sie anschliessend die *Enter* Taste um den Befehl auszuführen. 

9.  Der Router gibt eine Tabelle aus. Beantworten Sie die nachfolgenden Fragen:
 - Welche IP-Adresse ist auf dem *Interface* *GigabitEthernet0/0* und *GigabitEthernet0/1* konfiguriert?
 - Ist das *Interface* *GigabitEthernet0/2* aktiv? Welchen Status hat es?
 - Wo ist das *Interface* *GigabitEthernet0/1* in der Netzwerkgrafik zu finden?

## 3. Konfiguration anzeigen
Im letzten Abschnit wurde mit dem Befehl `show ip interface brief` die konfigurierten IP-Adressen auf den *Interfaces* des Routers angezeigt. In den nachfolgenden Abschnitten lernen wir weitere Befehle kennen, um verschiedene Arten von Informationen auszulesen. 

In diesem Abschnitt lernen wir den Befehl `show running-config` kennen. Dieser Befehl ist äusserst hilfreich, da er die gesamte getätigte Konfiguration ausgibt. Dabei gilt es zu beachten: Alles was nicht explizit konfiguriert worden ist, verbleibt in der  *default configuration*.

Versuchen Sie ein Gefühl für die Konfiguration zu erhalten, indem Sie die nachfolgenden Fragen beantworten:
 - Mit welchem Befehl (wie lautet die Zeile) wurde der Name des Routers konfiguriert?
 - Wie lautet das Passwort für den *remote access* mit TELNET?

## 4. DHCP Konfiguration
Ziel dieses Abschnittes ist es die DHCP Konfiguration des Routers auszulesen. Von der theorie zu DHCP wissen wir, dass folgende Parameter benötigt werden: 
 - *ip address pool*: IP-Address Bereich, der an DHCP-Clients vergeben wird.
 - *default gateway*: Zusätzliche Information für den Client: Das zu verwendende Standard Gateway
 - *dns nameserver ip address* (theoretisch nicht zwingend notwendig, aber praktisch immer konfiguriert)

Ein DHCP Server muss zudem wissen für welches Subnetz bzw. Interface er den Dienst zur verfügung stellen soll.

Vervollständigen Sie folgende Key-Value Liste:
```
network: 
ip address pool: 
default gateway: 
dns nameserver:
interface:
```

Folgende Befehle sind dabei hilfreich:
 - `show running-config`
 - `show ip dhcp pool`
 
Beantworten Sie abschliessend die Frage:
 - Welche IP-Adresse wird dem ersten DHCP-Client vergeben, der einen *DHCP request* macht?

## 5. DHCP lease
In diesem Abschnitt haben wir das Ziel einen *DHCP lease* zu beobachten und die relevanten IP-Packet in einer PCAP Datei abzuspeichern. 

1. Im Kontextmenü der Verbindung zwischen PC1 und Switch1 *Start capture* wählen. Im Dialog anschliessend *OK* drücken. Wireshark öffnet sich. 
2. Starten Sie nun den PC1. Wählen dafür in dessen Kontextmenü *Start*. 
3. Identifizieren Sie die für den *DHCP lease* relevanten Packete.
4. Schauen Sie sich die Packete ein wenig genauer an indem Sie nachfolgende Fragen bearbeiten:
- Welche IP Adresse wir dem Client zugeteilt?
- Nach welcher Zeit müssen die Clients den *DHCP lease* erneuern?
- Welche Option Nummer hat die Option "Router"?
- Welche Option Nummer hat die Option "Domain Name Server"?
5. Markieren Sie die relevaten Packete und exportieren Sie NUR diese in ein PCAP file. 
6. **Die PCAP Datei ist dem eigenen Portfolio hinzuzufügen.**

## 6. VPCS
Öffnen Sie die Konsole des PC1, probieren Sie nachfolgende Befehle aus. Beschreiben Sie für jeden Befehl dessen Funktion und den Inhalt des Outputs. Pro Befehl maximal 1 bis 3 Sätze. Bei den drei Ping Befehlen ist der unterschied zu erklären. 

 - `show ip`
 - `dhcp`
 - `ping 192.168.10.1`
 - `ping google.ch`
 - `ping 8.8.8.8`

## 7. PCs konfigurieren
Ziel: Alle PCs (PC1,PC2,PC3) fordern automatisch beim Boot per DHCP eine IP-Adresse an. 

*Für diese Übung gibt es keine Schritt-für-Schritt Anleitung. Versuchen Sie selbstständig herauszufinden, welche Schritte notwendig sind, um das oben aufgeführte Ziel zu erreichen.*

## 8. Cisco CLI Befehl herausfinden - DHCP bindings
Ziel: Screenshot mit dem Output des Cisco CLI Befehls, der alle an DHCP Clients verteilte IP Adressen anzeigt. Auf dem Screenshot ist der Befehl und die drei *leases* der PCs sichtbar. 

*Für diese Übung gibt es keine Schritt-für-Schritt Anleitung. Versuchen Sie selbstständig herauszufinden, welche Schritte notwendig sind, um das oben aufgeführte Ziel zu erreichen.*

## 9. Ubuntu Desktop Guest
Ziel: Erweiterung des GNS3 Projekt Netzwerkes um eine *Ubuntu Desktop Guest VM*. Zugriff mit der *Ubuntu Desktop Guest VM* mit Firefox auf das Internet. 

Im Lernjournal sind folgende Screenshots einzufügen:
 - Screenshot des Netzwerkes inklusive der Ubuntu Desktop Guest VM. 
 - Screenshot aus der Detailanzeige des Netzwerkinterfaces von der Ubuntu Oberfläche (Mit Fenster in dem die aktuelle IPv4-Adresse, Default Route, usw. angezeigt werden kann.)

*Für diese Übung gibt es keine Schritt-für-Schritt Anleitung. Versuchen Sie selbstständig herauszufinden, welche Schritte notwendig sind, um das oben aufgeführte Ziel zu erreichen.*

Können Sie mit dem Firefox der Ubuntu VM auf https://watson.ch zugreifen?

**Hinweise:**
 - Das Gerät für *Ubuntu Desktop Guest* ist im GNS3 im Menü Links zu finden
 - Die *Ubuntu Desktop Guest VM* ist mit dem *Switch1* zu verbinden.
 - Logindaten: Benutzername: osboxes.org Passwort: osboxes.org
 - Bei Problemen mit Sonderzeichen in der VM: Stellen Sie das Tastaturlayout ihres Laptops auf *US Keyboard* um. 

# Weiterführende Ressourcen 
 - Cisco CLI for Beginners: https://www.youtube.com/watch?v=xOqwxluUCc8 (Für GNS3 Relevante CLI Infos Ab 08:19)
 - Using Cisco IOS Command Line: https://www.cisco.com/c/en/us/td/docs/ios-xml/ios/fundamentals/configuration/15mt/fundamentals-15-mt-book/cf-cli-basics.html 