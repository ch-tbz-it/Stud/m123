# DHCP

In der Welt von TCP/IP steht das Wort *host* für ein Gerät mit einer IP-Adresse: Ein Tablet, Laptop, PC, Server, Router Switch - jedes Gerät das IP Services verwendet oder anbietet, benötigt eine IP Adresse. 

Ein Host der IPv4 verwendet benötigt diese vier IPv4 Einstellungen um "korrekt" zu funktionieren:
 - IP-Adresse
 - Subnetzmaske
 - Default Gateway
 - DNS Server IP-Adresse

Was bedeutet in diesem Zusammenhang "korrekt"?
 - **IP-Adresse und Subnetzmaske** werden für die Kommunikation innerhalb des Subnetzes benötigt.<br><i>Ein PC mit der Adresse 192.168.1.32/24 kann damit mit allen Geräten von 192.168.1.1 bis 192.168.1.255 kommunizieren</i>
 - Das **Default Gateway** benötigt ein Host, um mit Geräten ausserhalb seines Subnetzes kommunizieren zu können.<br><i>z.B. beim Befehl `ping 8.8.8.8` befindet sich die IP-Adresse `8.8.8.8` ausserhalb des Subnetzes 192.168.1.0/24. Der Host sendet das IP-Paket an das Default Gateway.</i>
 - Die **IP-Adresse des DNS Servers** wird benötigt, damit der Host öffentlich bekannte Domainnamen wie z.b. www.tbz.ch oder www.tagesanzeiger.ch mithilfe des DNS Protokolls in IP-Adressen auflösen kann. 

Das *Dynamic Host Configuration Protocol* (DHCP) ist eines der meistgenetzen Services im TCP/IP Netzwerk. Die meisten Hosts verwenden DHCP um ihre IPv4 Einstellungen zu erlernen. Der grosse Vorteil von DHCP gegenüber dem manuellen Konfigurieren der IPv4-Einstellungen auf jedem Host: Die Host erlernen ihre IPv4-Einstellungen automatisch. Ein manuelles Konfigurieren jedes einzelnen Hosts entfällt. 

Heutzutage versuchen Hosts, die sich mit einem LAN oder WLAN verbinden, standardmässig Ihre IPv4-Einstellungen mithilfe von DHCP zu erhalten. 

# Theoretische Grundlagen

Bevor mit den Laborübungen begonnen wird, empfiehlt es sich in die Thematik einzuarbeiten. Dafür stehen in der Dokumentenablage zum Modul (Meistens in der MS Teams Gruppe unter Allgemein -> Dateien/Dokumente -> Kursmaterialien) verschiedene Quellen zur Verfügung. 

Eine Vielzahl von Erklär-Videos zum Thema finden Sie auf [Youtube](https://www.youtube.com/results?search_query=DHCP). 



## Ablauf eines DHCP-Requests

Nachfolgend sind die vier Phasen eines DHCP-Requests vereinfacht dargestellt. Dabei gilt zu beachten, dass es sich hier um einen *optimalen Verlauf* handelt. D.h. es gehen keine Packete verloren, der Client ist mit jedem Offer einverstanden und es sind keine speziellen Optionen konfiguriert. 

### Step 1 - DHCP Discover
![DHCP Discover simplified](media/simplified_step_1.PNG)

### Step 2 - DHCP Offer
![DHCP offer simplified](media/simplified_step_2.PNG)

### Step 3 - DHCP Request
![DHCP request simplified](media/simplified_step_3.PNG)

### Step 4 - DHCP Acknowledge
![DHCP acknowledge simplified](media/simplified_step_4.PNG)

## Weitere Links
 - https://learn.microsoft.com/en-us/windows-server/troubleshoot/dynamic-host-configuration-protocol-basics
 - https://www.ip-insider.de/was-ist-dhcp-a-590923/
 - http://www.netzmafia.de/skripten/netze/netz9.html



## Selbstevaluation
Wenn nachfolgende Fragen beantwortet werden können, sollten ausreichend Kenntnisse zum lösen der Laborübungen zur Verfügung stehen. 

1. Welcher manueller Konfigurationsprozess entfällt durch den Einsatz von DHCP?
2. An welche IP-Adresse sendet der DHCP-Client den DHCPDISCOVER? Wie lautet die Bezeichnung für die Art dieses IP-Packetes?
3. Stellen Sie den DHCP-Anforderungsprozess zwischen dem DHCP-Client und dem DHCP-Server grafisch dar.
4. Wie kann es trotz Einsatz eines DHCP-Servers zu einem IP-Konflikt kommen?

# Laborübungen mit Packet Tracer
Die nachfolgenden Laborübungen bauen aufeinander auf. Denken Sie daran die Laborübungen gemäss Anweisungen in [02_Lernjournal](../02_Lernjournal) zu dokumentieren (Beachte Abweichende Anweisungen des Kursleiters/Lehrperson).

## Laborübung 1: DHCP Server Konfiguration aus einem Cisco Router auslesen
[Auf zum Labor 1 (Packet Tracer)](DHCP_PacketTracer/01_DHCP.md)


# Laborübungen mit GNS3 (Fortgeschritten)
Die nachfolgenden Laborübungen bauen aufeinander auf. Denken Sie daran die Laborübungen gemäss Anweisungen in [02_Lernjournal](../02_Lernjournal) zu dokumentieren, sofern vom Kursleiter / Lehrperson gefordert.

## Laborübung 1: DHCP Server Konfiguration aus einem Cisco Router auslesen
[Auf zum Labor 1 (GNS3)](DHCP_GNS3/01_DHCP%20Server%20Konfiguration%20Cisco.md)

## Laborübung 2: DHCP Server auf einem MikroTik Router
[Auf zum Labor 2 (GNS3)](DHCP_GNS3/02_DHCP%20Server%20auf%20MikroTik%20Router.md)


