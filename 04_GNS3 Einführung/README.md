# GNS3 Einführung

 GNS3 ist ein leistungsstarker und umfangreicher Netzwerk Emulator, der es erlaubt echte Networking-Software auszuführen. Vielleicht haben Sie bereits mit VMWare, VirtualBox oder Hyper-V gearbeitet. Diese Produkte erlauben es Ihnen Betriebssysteme wie Microsoft Windows, Linux oder sogar Mac OS X in einer virtuellen Maschine auf ihrem Computer auszuführen. GNS3 lässt Sie dasselbe tun mit Cisco IOS, Juniper Routers, MikroTik und einigen weiteren Herstellern. Zusätzlich hat es ein grafisches Frontend mit dem Sie die emulierte Netzwerktopologie erstellen und visualisieren können.

 Die GNS3 Einführung besteht aus zwei Teilen:
 1. GNS3 Installation und Einrichtung auf Ihrem persönlichen Laptop
 2. GNS3 Einführungsübungen `Zwei VPCs pingen sich gegenseitig` und `Mein PC pingt ins Labor`

Die Beschreibung dazu befindet sich in den Unterlagen des Moduls 129.

**Wichtig: Die Repositories lassen sich leicht verwechseln. Im welchen Modulunterlagen man sich gerade befindet, lässt sich am besten im Link sehen.**

**Wichtig: Lösen Sie nur Aufgaben in Unterlagen von anderen Modulen, die explizit gefordert und verlinkt sind. Fragen Sie bei Unklarheiten die Lehrperson.**

## Links zu den Unterlagen im Modul 129
 - GNS3 Labor installieren: <br> https://gitlab.com/ch-tbz-it/Stud/allgemein/tbzcloud-gns3/-/tree/main/02_Bedienungsanleitung/01_GNS3%20Installation 
 - Einführungsübungen: <br> https://gitlab.com/ch-tbz-it/Stud/allgemein/tbzcloud-gns3/-/tree/main/02_Bedienungsanleitung/02_GNS3%20Einf%C3%BChrung


