# Grundlagen

![Serverdienste im Netzwerk](media/Serverdienste_im_Netzwerk.PNG)

In einem [Computernetzwerk](https://en.wikipedia.org/wiki/Computer_network) stellen Server den Clients Dienste (Services) zur Verfügung. Die Dienste können von Clients (Windows-PC, MacBook, Android-Smartphone, usw.) lokal im Netzwerk oder von Server in der [Cloud](https://en.wikipedia.org/wiki/Cloud_computing) (~= Datencenter) bereitgestellt werden. Es gibt auch Dienste die keinen zentralen Server benötigen (Beispiel: [Peer-To-Peer Protokolle](https://en.wikipedia.org/wiki/Peer-to-peer)  (distributed application)). In diesem Abschnitt beschränken wir uns auf **lokale** Serverdienste, die auf dem **[Client-Server](https://en.wikipedia.org/wiki/Client%E2%80%93server_model)**-Model basieren. Ein bekanntes Beispiel ist ein [Dateiserver](https://en.wikipedia.org/wiki/File_server), der seinen Clients mithilfe von Dateiübertragungsprotokolle (z.B. [SMB](https://en.wikipedia.org/wiki/Server_Message_Block)) eine zentrale Datenablage zur Verfügung stellt. Ein physischer oder virtueller Server (oder ein Container) kann einen oder mehrere Serverdienste anbieten. 

Die oben dargestellte Netzwerktopologie zeigt den typischen Aufbau eines Firmennetzwerkes. 

## Server, was ist das?
Auf der einen Seite beschreibt der Begriff Server die Hardware. Diese kann ein normaler PC sein. Bei grösseren Installationen, wie z. B. Webservern oder Fileservern, ist die Hardware umfangreicher ausgelegt. Die verbauten Teile sind grösser dimensioniert und ausserdem sind die Komponenten (z. B. die Festplatte) auf den Dauerbetrieb ausgelegt.

Auf der anderen Seite beschreibt der Begriff Server aber auch die Software. Es gibt Server für die unterschiedlichsten Anwendungen:
 - Webserver können HTML-Seiten an Clients senden und Scriptsprachen ausführen.
 - Mailserver stellen die gängigen eMail-Funktionen zur Verfügung.
 - DHCP-Server verwalten und verteilen IP-Adressen.
 - LDAP-Server ermöglichen eine zentrale Benutzerverwaltung in einem Netzwerk.
 - DNS-Server: Seine Hauptaufgabe ist die Beantwortung von Anfragen zur Namensauflösung.
 - uvm.

Für alle diese Anwendungen gibt es Programme, die eine Dienstleistung zur Verfügung stellen. Wir sprechen von Serverdiensten. Dabei können auch alle diese Programme auf einem Computer bzw. Server laufen. Dieser bietet dann alle diese Serverdienste an.
Im folgenden Modul widmen wir uns einigen dieser Dienste. Es ist nicht möglich in dieser kurzen Zeit sämtliche möglichen Dienste anzuschauen. Wir beschränken uns daher auf: 
 - DHCP - Dynamic Host Configuration Protocol
 - DNS - Domain Name System
 - Dateidienste

Eine Auswahl von Serverdiensten und den dazugehörigen Protokollen:
| Rolle              | Verwendung                                                                                                                                                                                       | Wichtigste Protokolle                                          |
|--------------------|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|----------------------------------------------------------------|
| Echo Reply         | Antwortet auf ICMP Echo-Reply. Meist Teil des Betriebssystems.                                                                                                                                   | ICMP                                                           |
| DNS-Server         | Host-Namensauflösung und Verwaltung der DNS-Namensräume.                                                                                                                                         | DNS (UDP, 53)                                                  |
| DHCP-Server        | Zentrale Verwaltung von IP-Konfigurationen und Zuteilung von IP-Adressen an Host-Systeme.                                                                                                        | DHCP (UDP 67, 68)                                              |
| Domänen-Controller | Server, der den Zugang in einer Netzwerk-Domäne zusammengefassten Computern regelt. Stellt ein Verzeichnisdienste zur zentralen Authentifizierung, Authorisierung und Überwachung zur Verfügung. | LDAP (389), LDAPs (636), SMB (TCP 139, 445), Kerberos (UDP 88) |
| Webserver          | Stellen Informationen (Webseiten, Web-Applikationen) über http zur Verfügung.                                                                                                                    | HTTP (80), HTTPs (443)                                         |
| Dateiserver        | Stellen Dateien und Verzeichnisstrukturen zur Verfügung                                                                                                                                          | SMB (TCP 139, 445)<br/>FTP (TCP 20, 21)<br/>AFP (TCP 548)      |
| Printserver        | Stellen Druckerdienste zur Verfügung und verwalten die Druckerwarteschlangen (Spooling). Zentrale stelle für Druckerverwaltung und Treiber.                                                      | SMB, http, IPP (TCP 631), LDP                                  |

(Diese Tabelle ist nicht abschliessend.)

## Grundsätzlich
Ein Server ist ein Computer oder virtuelles System, das Ressourcen und Rechenleistung bereitstellt, um andere Geräte im Netzwerk – die sogenannten Clients – zu bedienen. Dabei kann der Begriff Server sowohl die Hardware als auch ein Computerprogramm beschreiben. Der Serverdienst ist die Software, die auf einem Server (oder auch auf einem Client) läuft und bestimmte Funktionen wie Dateifreigabe, E-Mail oder DNS anbietet. Ob ein Gerät als Server oder Client betrachtet wird, hängt dabei vom Haupteinsatzweck ab: Ein Server ist primär für die Bereitstellung von Diensten zuständig, während ein Client hauptsächlich auf diese zugreift.

## Server als Software
Ein Server als Software ist ein Programm, das im Client-Server-Modell mit einem anderen Programm, dem Client (englisch für Kunde), kommuniziert. Der Client kann dadurch bestimmte, von der Server-Software bereitgestellte Funktionalitäten nutzen, zum Beispiel Dienstprogramme, Netzwerkdienste, den Zugang zu einem Dateisystem oder eine Datenbank.

## Server als Hardware

Serverhardware ist auf Zuverlässigkeit und Performance unter Dauerbelastung ausgelegt. Zu den Schlüsselkomponenten gehören Prozessor (CPU), Arbeitsspeicher (RAM), Speicher (Storage), Netzwerkkarte und redundante Stromversorgung.

Die CPU beeinflusst massgeblich die Geschwindigkeit und Anzahl gleichzeitiger Prozesse, wobei mehr Kerne und Threads für parallele Verarbeitung wichtig sind. Der Arbeitsspeicher (oft als ECC-RAM zur Fehlerkorrektur) unterstützt ebenfalls parallele Prozesse und verhindert Datenfehler. Beim Speicher unterscheidet man zwischen günstigeren Festplatten (HDDs), die pro GB weniger kosten, und schnellen NVMe-SSDs. HDDs erscheinen zunächst wirtschaftlich vorteilhaft, sind jedoch oft nicht ideal für datenintensive Anwendungen, die hohe Zugriffsraten und geringe Latenzen erfordern. Hier bieten NVMe-SSDs trotz höherer Anschaffungskosten ein besseres Kosten-Nutzen-Verhältnis durch schnellere Ladezeiten und höhere Effizienz, was insbesondere für datenintensive Serverdienste wichtig ist.

Die Datenübertragungsrate der Netzwerkkarte beeinflusst, wie viele Clients der Server gleichzeitig bedienen kann. Die Performance hängt stark von den Anforderungen der Anwendung ab. Beispielsweise erfordert ein Videostreaming-Dienst eine hohe, kontinuierliche Bandbreite pro Client, da grosse Datenmengen für jedes gestreamte Video in Echtzeit übertragen werden müssen. Es bringt nichts, eine Server mit Leistungsfähiger CPU und viel RAM zu haben, wenn die Netzwerkanbindung zu klein dimensioniert ist. Die Anforderungen an die Netzwerkkarte variieren daher stark je nach Art der Anwendung, und die Hardwareausstattung sollte entsprechend angepasst werden.

Zusätzlich sollte der Stromverbrauch der Hardware bedacht werden, da Server oft 24/7 laufen und eine effiziente Stromversorgung sowie Kühlung erforderlich ist. Redundante Netzteile verbessern die Ausfallsicherheit, erhöhen jedoch den Strombedarf, was die Betriebskosten steigern kann.

Wirtschaftlich gesehen sollte die Hardware gezielt auf den Einsatzzweck abgestimmt sein, um Kosten bei Anschaffung und Betrieb effizient zu halten. Serverhardware muss dabei auf den geplanten Einsatzbereich – sei es als Web-, Datenbank- oder Virtualisierungsserver – abgestimmt werden, um ein optimales Kosten-Nutzen-Verhältnis und langfristige Wirtschaftlichkeit zu gewährleisten.

Es ist ausserdem entscheidend, die Hardware so zu dimensionieren, dass sie die geplante Nutzungsdauer problemlos abdeckt. Das bedeutet, dass die Hardware bei steigender Nutzung, beispielsweise durch mehr Mitarbeiter, die auf den Dateiserver zugreifen, nicht sofort an ihre Leistungsgrenzen stösst. Stattdessen sollten Kapazitätsreserven eingeplant werden, um wachsende Anforderungen und eine steigende Nutzerzahl abzufangen. Einsparungen bei der Serverhardware lohnen sich nicht, wenn die Systeme bereits nach einem Jahr ausgetauscht werden müssen. Durch eine vorausschauende Dimensionierung kann die Hardware längerfristig effizient genutzt und an die steigenden Anforderungen angepasst werden.

Übung: [Serverhardware für Anwendungsfall auswählen](./01_Serverhardware/README.md)

## Formen von Server
Ein Server kann unterschiedlich konfiguriert sein, um den spezifischen Anforderungen der Nutzer und Anwendungen gerecht zu werden. Hierbei gibt es drei Haupttypen von Servern:

 - Virtuelle Server: Ein leistungsfähiger Host (physischer Server) betreibt mehrere virtuelle Server, die für Nutzer wie eigenständige Server wirken.
 - Dedizierte Server: Ein Server ist exklusiv einem Netzwerkdienst zugeordnet und wird nicht für andere Aufgaben genutzt.
 - Shared Server: Mehrere Kunden teilen sich die Ressourcen eines Servers.

## Client-Server Modell
![Client-Server-Modell](media/client-server.png)

"Der Server stellt einen Service bereit. Clients können diesen Service in Anspruch nehmen."

Der Server stellt den Clients Dienste zur Verfügung. Der Server kann dabei innerhalb oder ausserhalb des Netzwerks liegen. Einer der am häufigsten verwendeten Dienste ist der Webserver. Dieser stellt seinen Clients Webseiten und/oder Webapplikationen über das HTTP-Protokoll zur Verfügung. Wichtige Eigenschaften des Client-Server-Modells:

 - Ein zentraler Server stellt Dienste zur Verfügung.
 - Clients verbinden sich über ein oder mehrere Netzwerke (auch bekannt als Internet) mit dem Server.
 - Ein Server kann gleichzeitig mehrere Clients bedienen.

## Theoretische Grundlagen
In der Dokumentenablage zum Modul stehen (Meistens in der MS Teams Gruppe unter Allgemein -> Dateien/Dokumente -> Kursmaterialien) verschiedene Ausschnitte aus Fachbücher und Skripte zur Verfügung.

# Weiterführende Ressourcen
 - [Wichtigste Kommandozeilenbefehle](01_Wichtigste_Befehle.md)
 - [Wikipedia - Server](https://en.wikipedia.org/wiki/Server_(computing))