# Übung: Serverhardware auswählen

## Kompetenzen
 - A1G: Ich kann wesentliche Merkmale von Serverhardware aufzählen. 
 - A1F: Ich kann den Einfluss von einzelnen Hardwarekomponenten auf die Performance erklären. 
 - A1E: Ich kann für einzelne Serverdienste sinnvolle Hardwareausrüstung empfehlen. 

## Aufgabenstellung
Halte alle deine Überlegungen in einem Dokument fest, dass du anschliessend mit anderen Lernenden und abschliessend mit der Lehrperson besprechen kannst. 

1. Überlege dir einen Einsatzzweck für einen Server (z.B. Windowsdateiserver für 20 User, Minecraft Gaming Server, Virtualisierungshost für mehrere virtuelle Server). 
2. Überlege dir was für Serverdienste auf dem Server laufen werden. 
3. Informiere dich darüber, welche Leistungsanforderungen die einzelnen Dienste haben.
4. Lege fest wie viel Leistung dein Server benötigt. Beachte dabei, dass die Serverhardware üblicherweise 3 bis 7 Jahre im Betrieb ist und deine Auswahl Nachhaltig sein muss.
5. Informiere dich selbstständig (z.B. Internet, oder Person mit Erfahrung fragen) wie lange Serverhardware in der Regel im Einsatz ist. Berechne, was der Server pro Jahr kosten wird. 
6. Stelle die Serverhardware zusammen und begründe deinen Entscheid. 
7. Gehe die unten aufgeführten Kriterien durch: Erfüllt dein Resultat alle Kriterien?

Bespreche das Resultat mit der Lehrperson. 

## Kriterien
Die nachfolgenden Kriterien helfen dir dein Resultat zu überprüfen. 

 - Der Anwendungszweck für den Server ist realistisch gewählt. 
 - Für jeden Dienst ist definiert, welche Anforderungen dieser an die Hardware hat. <br> *Was für einen Prozessor benötigt wird, wie viel RAM, Anforderungen an die Grafikkarte, usw.*
 - Die Leistung des ausgewählten Servers passt zum Anwendungszweck und erlaubt höchstwahrscheinlich eine Nutzung über die geplante Nutzungsdauer.
 - Die Hardwarekosten sind aufgeführt, der Nutzungszeitraum definiert und die jährlichen Kosten berechnet.
 - Der Entscheid ist in maximal 50 Worte nachvollziehbar begründet. 