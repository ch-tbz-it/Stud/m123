# Übung - Serverhardware dimensionieren

Serverhardware unterscheidet sich nicht nur durch Ihren Formfaktor von Consumer-Hardware. Was ist der Unterschied zwischen einem [HPE ProLiant Server](https://www.hpe.com/ch/de/hpe-proliant-servers.html) und einer [Lenovo ThinkStation](https://www.lenovo.com/ch/de/desktops/)?

## Kompetenzen
 - A1G: Ich kann wesentliche Merkmale von Serverhardware aufzählen. 
 - A1F: Ich kann den Einfluss von einzelnen Hardwarekomponenten auf die Performance erklären. 
 - A1E: Ich kann für einzelne Serverdienste sinnvolle Hardwareausrüstung empfehlen. 

## Einleitung
Bleiben wir zunächst bei der Frage der Definition. Welche Merkmale muss ein System aufweisen, um als Server bezeichnet zu werden? Der Begriff "Server" leitet sich vom englischen Verb to serve ab, was so viel bedeutet wie dienen oder bedienen. Diese grundlegende Funktion wird somit durch den Namen bereits beschrieben: Der Server erfüllt eine dienende Rolle.

Daraus ergeben sich zwei gängige Definitionen im IT-Kontext:
1. Server sind Anwendungen, die spezifische Dienste oder Programmfunktionen für andere Netzwerkteilnehmer bereitstellen oder ihnen Informationen zugänglich machen.
2. Server sind physische Geräte, die für die unter Punkt 1 beschriebenen Aufgaben optimiert sind.

## Der Begriff Server als Software
Server im Sinne von Software umfassen Betriebssysteme (z.B. Windows Server oder Debian ohne Desktop mit installierten Serverdienste) und spezialisierte Programme wie zum Beispiel Webserver oder Mailserver. Sie zeichnen sich durch ihre Zweckbestimmung (z. B. als Datenspeicher oder Mailserver), die Fähigkeit, mehrere Clients gleichzeitig zu bedienen, Skalierbarkeit, Sicherheitsmechanismen und Stabilität im Dauerbetrieb aus. Jede Serverrolle bringt spezifische Anforderungen mit sich, die für einen reibungslosen Betrieb erfüllt sein müssen. Dabei können Serversoftwarelösungen flexibel angepasst werden, um Leistung, Benutzerverwaltung oder Zugriffsrechte zu skalieren und Angriffe abzuwehren. Stabilität ist essenziell, damit Server auch bei intensiver Nutzung und unerwarteten Fehlern zuverlässig bleiben.

## Der Begriff Server als Hardware
Serverhardware wird speziell für den dauerhaften Einsatz und zukünftige Skalierung konzipiert. Wichtige Merkmale sind ausreichende Kapazität, Ausbaubarkeit (z. B. durch zusätzliche Prozessoren oder Speichermodule), physische Sicherheit und Redundanz zur Ausfallsicherheit. Systeme müssen robust sein, um Dauerbetrieb zu ermöglichen, einschliesslich hochwertiger Bauteile, effizienter Kühlung und schneller Verfügbarkeit von Ersatzteilen. Serverhardware kann auch durch Technologien wie Blade-Server und Clusterbildung erweitert werden, um Leistung und Skalierbarkeit den Anforderungen anzupassen. Sicherheit umfasst abschliessbare Gehäuse, Überwachungsmechanismen und die Auswertung von Logdaten.

## Warum ein PC kein Server ist – Unterschiede in Hardware und Design
Obwohl PCs und Server ähnliche Komponenten wie Prozessoren, Arbeitsspeicher und Festplatten nutzen können, unterscheiden sie sich grundlegend in Kapazität, Ausbaubarkeit, physischer Sicherheit, Dauerbetriebsfähigkeit und Lebenszyklusmanagement. Server bieten mehrere Prozessor- und RAM-Sockel, um höhere Leistungsanforderungen zu bewältigen, und sind für spezifische Kapazitäten hinsichtlich Platz, Bauweise und Lüftung konzipiert. Sie können leicht erweitert werden, beispielsweise durch zusätzliche Arbeitsspeicher oder Festplatten, und ermöglichen so eine flexible Anpassung an steigende Anforderungen.

## Zuverlässigkeit und Lebenszyklus von Servern
Serverhardware wird für den Dauerbetrieb optimiert, mit robusten Komponenten wie speziellen Festplatten und redundanten Systemen für Ausfallsicherheit, z. B. RAID-Konfigurationen oder Hotspare-Speicher. Servergehäuse sind grösser, um zusätzliche Kühlung zu ermöglichen, und Sicherheitsmassnahmen wie abschliessbare Gehäuse oder Überwachungssysteme sorgen für physischen Schutz. Zusätzlich legen Hersteller Wert auf langfristige Verfügbarkeit von Ersatzteilen und regelmässige Updates, etwa für BIOS oder Treiber, was eine stabile und einheitliche Serverinfrastruktur über längere Zeiträume sicherstellt – ein Aspekt, den PC-Systeme nicht in diesem Mass abdecken.

## Bauformen von Servern
Server werden in drei Hauptbauformen unterschieden: Tower-, Rack- und Blade-Server.

 - **Tower-Server** sind eigenständige Geräte, ideal für einzelne Serverinstallationen. Sie enthalten alle notwendigen Komponenten und sind für kleinere Umgebungen geeignet, jedoch aufgrund ihrer Grösse nur eingeschränkt stapelbar.
 - **Rack-Server** werden in standardisierte Regale (Racks) montiert, bieten höhere Dichte und ermöglichen effizientere Verkabelung und Kühlung. Sie eignen sich für grössere Installationen mit mehreren Servern.
- **Blade-Server** sind kompakte Module, die in einem gemeinsamen Gehäuse (Enclosure) innerhalb eines Racks betrieben werden. Sie teilen sich Funktionen wie Stromversorgung und Kühlung, sind besonders platzsparend und energieeffizient.

## KVM und Verwaltung
Für das Management mehrerer Server bieten KVM-Switches (Keyboard-Video-Mouse) eine zentrale Steuerungseinheit, die den Zugriff auf verschiedene Server über eine einzige Tastatur, Maus und einen Monitor ermöglicht. Dies wird vor allem benötigt, wenn eine Operation nicht direkt über das Netzwerk erfolgen kann. Moderne KVM-Lösungen nutzen IP-basierte Technologien, um Server auch aus der Ferne zu verwalten. Zudem verfügen viele Server über dedizierte Managementschnittstellen (z. B. iRMC, iLO), die direkten Zugriff auf Hardware-Management ermöglichen, auch ohne physische Anwesenheit, was die zentrale Verwaltung grosser Server-Infrastrukturen erleichtert.

## Verständnis- und Vertiefungsfragen
Die nachfolgenden Fragen dienen zur Vertiefung des Themas. 
Recherchieren weiter zum Thema und überlegen Sie sich in welche die wichtigsten Unterscheidungsmerkmale zwischen einem Enduser-PC und einem Rack Server sind. Nutzen Sie dafür folgende Fragen, um sich ins Thema einzuarbeiten. Halten Sie Ihre Antworten und tauschen Sie sich mit ihren Kollegen zum Thema aus:
 - In welchen Gehäusen werde Server und Enduser-PCs geliefert. Weshalb wird dieser Formfaktor gewählt?
 - Wie unterscheidet sich die Lüftung eines Servers und einem Enduser-PCs?
 - Was ist ECC und weshalb wird das in Servern häufig verwendet?
 - Weshalb werden in Servern teilweise andere SSDs verwendet als in Enduser-PCs?
 - Kann ein normaler Enduser-PC auch als Server eingesetzt werden? Wenn ja, weshalb wird trotzdem Serverhardware eingekauft, obwohl diese häufig teurer ist?
 - Wie unterscheidet sich eine Server-CPU (z.B. Intel Xeon) zu einem normalen Consumer-CPU (z.B. Intel Core i7)?

## Weitere Aufgaben

- [Für einen Anwendungsfall Serverhardware zusammenstellen](./01_Serverhardware%20auswaehlen.md)

