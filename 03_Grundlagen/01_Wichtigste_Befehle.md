# Wichtigste Kommandozeilenbefehle

# Allgemein
Eine aktive Kommandozeile (*terminal session*) in Linux (Unix), Windows oder macOS (Darwin => BSD) hat immer einen Kontext. Dieser besteht (unter anderem) aus *enviroment variables* (Umgebungsvariablen) und dem Verzeichnis aus dem man sich befindet. Alle Befehle werden immer in diesem Kontext ausgeführt. D.h. führt man z.B. den Befehl `ls` (Auflisten des Verzeichnisinhaltes) so enthält die Ausgabe den Inhalt für das aktuelle Verzeichnis. 

# Windows

## Version
```
C:\Users\user>ver

Microsoft Windows [Version 10.0.19044.2130]
```
Gibt die Windows Version zurück.

## netstat
```
C:\Users\user>netstat -a -o -n

Active Connections

  Proto  Local Address          Foreign Address        State           PID
  TCP    0.0.0.0:135            0.0.0.0:0              LISTENING       1228
  TCP    0.0.0.0:445            0.0.0.0:0              LISTENING       4
  TCP    0.0.0.0:1025           0.0.0.0:0              LISTENING       8
  TCP    0.0.0.0:2179           0.0.0.0:0              LISTENING       2416
  TCP    0.0.0.0:5040           0.0.0.0:0              LISTENING       9704
  TCP    0.0.0.0:47546          0.0.0.0:0              LISTENING       4584
  ...
```
Gibt eine Tabelle mit allen offenen Verbindungen und *listening sockets* zurück. 

## IP Konfiguration anzeigen
```
C:\Users\philipp>ipconfig /all

Windows IP Configuration

   Host Name . . . . . . . . . . . . : DESKTOP-UZGHIW
   Primary Dns Suffix  . . . . . . . :
   Node Type . . . . . . . . . . . . : Mixed
   IP Routing Enabled. . . . . . . . : No
   WINS Proxy Enabled. . . . . . . . : No
   DNS Suffix Search List. . . . . . : 

Wireless LAN adapter WiFi:

   Connection-specific DNS Suffix  . :
   Description . . . . . . . . . . . : Intel(R) Wireless-AC 9560 160MHz
   Physical Address. . . . . . . . . : 8C-C6-81-77-95-C0
   DHCP Enabled. . . . . . . . . . . : Yes
   Autoconfiguration Enabled . . . . : Yes
   IPv6 Address. . . . . . . . . . . : 2a02:168:AA00:70:1176:11d9:116b:9fe1(Preferred)
   Temporary IPv6 Address. . . . . . : 2a02:168:AA00:70:1176:11d9:116b:9fe1(Preferred)
   Link-local IPv6 Address . . . . . : fe80::1176:11d9:116b:9fe1%8(Preferred)
   IPv4 Address. . . . . . . . . . . : 192.168.0.172(Preferred)
   Subnet Mask . . . . . . . . . . . : 255.255.255.0
   Lease Obtained. . . . . . . . . . : Dienstag, 1. November 2022 09:14:13
   Lease Expires . . . . . . . . . . : Dienstag, 1. November 2022 12:13:59
...
```
Zeigt die IP Konfiguration mit dazugehörigen Einstellungen für alle Netzwerkschnittstellen an. 

```
ipconfig /flushdns
```
Löscht den internen DNS Cache.

## getmac
```
C:\Users\user>getmac

Physical Address    Transport Name
=================== ==========================================================
00-15-DD-00-AA-BB   \Device\Tcpip_{4A8BF8AC-DC00-1234-1234-F9B8123BA5BC}
```
Tabelle mit allen MAC Adressen und dazugehörige Netzwerkschnittstellen

## arp
```
C:\Users\user>arp -a

Interface: 172.25.129.38 --- 0xcd
  Internet Address      Physical Address      Type
  172.25.128.1          00-15-DD-00-AA-B1     dynamic
  172.25.143.255        ff-ff-ff-ff-ff-ff     static
  224.0.0.22            01-00-5e-00-00-16     static
  224.0.0.251           01-00-5e-00-00-fb     static
  224.0.0.252           01-00-5e-00-00-fc     static
  239.255.255.250       01-00-5e-7f-ff-fa     static
  255.255.255.255       ff-ff-ff-ff-ff-ff     static
```
Gibt eine Liste allen gelernten IP Adressen zurück. 

## Verzeichnisinhalt zurückgeben
```
C:\Users\user>dir
 Volume in drive C has no label.
 Volume Serial Number is 786B-C655

 Directory of C:\Users\user

10/05/2022  11:19 PM    <DIR>          .
10/05/2022  11:19 PM    <DIR>          ..
10/05/2022  11:19 PM    <DIR>          3D Objects
10/05/2022  11:19 PM    <DIR>          Contacts
10/05/2022  11:19 PM    <DIR>          Desktop
10/05/2022  11:19 PM    <DIR>          Documents
10/05/2022  11:19 PM    <DIR>          Downloads
10/05/2022  11:19 PM    <DIR>          Favorites
10/05/2022  11:19 PM    <DIR>          Links
10/05/2022  11:19 PM    <DIR>          Music
10/05/2022  11:19 PM    <DIR>          Pictures
10/05/2022  11:19 PM    <DIR>          Saved Games
10/05/2022  11:20 PM    <DIR>          Searches
10/05/2022  11:19 PM    <DIR>          Videos
               0 File(s)              0 bytes
              14 Dir(s)  40,230,694,912 bytes free
```
Listet den Inhalt des aktuellen Verzeichnisses auf.

## Ping
```
C:\Users\philipp>ping -4 www.google.ch

Pinging www.google.ch [216.58.215.227] with 32 bytes of data:
Reply from 216.58.215.227: bytes=32 time=2ms TTL=115
Reply from 216.58.215.227: bytes=32 time=1ms TTL=115
Reply from 216.58.215.227: bytes=32 time=2ms TTL=115
Reply from 216.58.215.227: bytes=32 time=3ms TTL=115

Ping statistics for 216.58.215.227:
    Packets: Sent = 4, Received = 4, Lost = 0 (0% loss),
Approximate round trip times in milli-seconds:
    Minimum = 1ms, Maximum = 3ms, Average = 2ms
```
Sendet einen *ICMP ECHO REQUEST* an den definierten Host und protokolliert die erhaltenen Antworten. Dieser Vorgang wird 4 mal wiederholt. 
Zusatzoption (Argument) `-n` führt so lange Wiederholungen aus, bis der Benutzer den Vorgang mit `Ctrl + c` abbricht. 

Das Argument `-4` zwingt `ping` den *ICMP ECHO REQUEST* mit IPv4 durchzuführen. IPv6 wird unter Windows und Linux standardmässig bevorzugt. Die Option ist hinfällig wenn eine IPv4 Adresse mitgegeben wird. 

Beispiel:
```bat
C:\Users\user> ping tbz.ch
```

## Windows Firewall
`wf`

Kurzbefehl, um die Windows-Firewall-Konfiguration aufzurufen

# Linux (Debian Derivate)
Alle Befehle werden anhand eines Beispiels gezeigt. 

Die Befehls-Eingabezeile setzt sich wie folgt zusammen:
```
root          @debian:  ~                     #  uname             -a
  ^              ^      ^                         ^                 ^
Benutzername  Host     Aktuelles Verzeichnis     Befehl/Programm   Argumente
```

Alle weiteren Zeilen nach der Befehls-Eingabezeile enthalten die Ausgabe (output) des Befehls bzw. Programmes.

Mit dem Befehl `man XYZ` (XYZ ist mit dem gewünschen Befehl zu ersetzen, Bsp: `man ls`) kann das *Manual* aufgerufen werden. Viele Befehle besitzen auch eine integrierte Hilfe, die meistens mit den Argumenten `-h` oder `--help` aufgerufen werden können. 

## Version
Beispiel:
```
root@debian:~# uname -a
Linux debian 5.10.0-16-cloud-amd64 #1 SMP Debian 5.10.127-1 (2022-06-30) x86_64 GNU/Linux
root@debian:~# cat /etc/issue
Debian GNU/Linux 11 \n \l
```
Diese beiden Befehle geben alle Informationen zum Linux Kernel und zum Linux Derivat zurück

## Eigenen Benutzernamen anzeigen
```
root@debian:~# whoami
root
```

## Verzeichnisinhalt auflisten
```
root@debian:~# ls -lha /
total 68K
drwxr-xr-x  18 root root 4.0K Nov  8 09:20 .
drwxr-xr-x  18 root root 4.0K Nov  8 09:20 ..
lrwxrwxrwx   1 root root    7 Jul 11 05:01 bin -> usr/bin
drwxr-xr-x   4 root root 4.0K Jul 11 05:05 boot
drwxr-xr-x  14 root root 2.8K Nov  8 09:20 dev
drwxr-xr-x  68 root root 4.0K Nov  8 09:20 etc
drwxr-xr-x   3 root root 4.0K Nov  8 09:20 home
...
```

## Terminalausgabe leeren
```
root@debian:~# clear
```

## Verfügbarer Speicherplatz auf Volumes anzeigen
```
root@debian:~# df -h
Filesystem      Size  Used Avail Use% Mounted on
udev            229M     0  229M   0% /dev
tmpfs            48M  392K   48M   1% /run
/dev/sda1       1.9G  620M  1.1G  36% /
tmpfs           238M     0  238M   0% /dev/shm
tmpfs           5.0M     0  5.0M   0% /run/lock
/dev/sda15      124M  5.9M  118M   5% /boot/efi
tmpfs            48M     0   48M   0% /run/user/1000
```
Gibt den verfügbaren Speicherplatz für die einzelnen *volumes* zurück. Die Angaben zur "Festplatte" sind hier in der Zeile `/dev/sda1       1.9G  620M  1.1G  36% /` zu finden: Das *volume* ist `1.9G` gross und hat `1.1G` frei. 

Wird das Argument `-h` (steht für *human*) weggelassen wird werden in grössenangaben in Bytes zurückgegeben.

## Verzeichnis wechseln
```
root@debian:~# cd /etc
root@debian:/etc# pwd
/etc
root@debian:/etc# cd /
root@debian:/# pwd
/
root@debian:/# cd
root@debian:~# pwd
/root
```
 - Mit dem Befehl `cd` kann das aktuelle Verzeichnis gewechselt werden. 
 - Der Befehl `pwd` zeigt das aktuelle Verzeichnis an. 

## IPv4 Adressen ausgeben
```
root@debian:~# ip --brief address
lo               UNKNOWN        127.0.0.1/8 ::1/128
ens4             UP             192.168.4.129/24 fe80::eb9:e1ff:fec5:0/64
```
Gibt alle konfigurierten IP-Adressen pro Netzwerkschnittstelle (*interface*) zurück. 


## IPv4 Routen anzeigen
```
root@debian:~# ip route
default via 192.168.4.1 dev ens4
192.168.4.0/24 dev ens4 proto kernel scope link src 192.168.4.129
```

## Ping
```
ping
```

## Dateieditor nano
![nano](media/nano.PNG)

Der Dateieditor `nano` ermöglicht das Bearbeiten von Textdateien. Er ist insbesondere für Beginner einfacher als `vim`. 

Tipp: Die unteren beiden Zeilen stehen für Kurzbefehle. `^X Exit` steht für: Mit `Ctrl + X` kann der Befehl `Exit` ausgeführt werden, bzw. das Programm beendet werden. 

## Datei Editor vi(m)
![vim](media/vim.PNG)

`vim` ist mächtiger Text- und Codeeditor. Er gehört zu den zwingenden Grundkenntnissen jedes Linux Administrators. Im Vergleich zu `nano` ist der Einstieg ein wenig schwieriger. 

**Randbemerkung:** Wenn jemand angibt Linux zu beherrschen lässt sich das einfach prüfen: Prüfen Sie die `vim` Kenntnisse dieser Person. Kann die Person `vim` schnell und auswendig bedienen, dann hat er bereits viele (Konfigurations)dateien bearbeitet und hat in der Vergangenheit die Motivation aufgebracht sich mit den Eigenheiten von `vim` zu beschäftigen. 

## Linux Command Cheat Sheet
Unter den nachfolgenden Seiten sind *Cheat Sheets* mit weiteren nützenlichen und häufig verwendeten Befehlen zu finden:
 - https://www.guru99.com/linux-commands-cheat-sheet.html
 - https://cheatography.com/davechild/cheat-sheets/linux-command-line/